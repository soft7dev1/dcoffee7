/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poc;

import com.mukku.dcoffee7.dao.CategoryDao;
import com.mukku.dcoffee7.dao.ProductDao;
import com.mukku.dcoffee7.model.Product;

/**
 *
 * @author user
 */
public class TestProduct {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ProductDao productDao = new ProductDao();
        System.out.println(productDao.get(1));
//        System.out.println("");
//        for (Product pd : productDao.getAll("product_type = 'CS' ", "product_id desc")) {
//            System.out.println(pd);
//        }
//        System.out.println("");
//        for (Product pd : productDao.getAll("product_id desc")) {
//            System.out.println(pd);
//        }
//        CategoryDao id = new CategoryDao();
//        Product newProd = new Product("เค้กแมว", "-", "-", 200, id.get(6));
//        productDao.save(newProd);
        Product pro1 = productDao.get(8);
        pro1.setName("เค้กแมวส้ม");
        productDao.update(pro1);
        productDao.delete(pro1);
        System.out.println("");
        for (Product pd : productDao.getAll()) {
            System.out.println(pd);
        }
    }

}
