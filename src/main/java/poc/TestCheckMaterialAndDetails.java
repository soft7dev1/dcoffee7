/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package poc;

import com.mukku.dcoffee7.dao.ChMatDetailDao;
import com.mukku.dcoffee7.dao.CheckMatDao;
import com.mukku.dcoffee7.dao.EmployeeDao;
import com.mukku.dcoffee7.dao.MaterialDao;
import com.mukku.dcoffee7.model.ChMaterialDetail;
import com.mukku.dcoffee7.model.CheckMaterial;
import com.mukku.dcoffee7.model.Employee;
import com.mukku.dcoffee7.model.Material;
import java.util.Date;
import org.sqlite.date.FastDateFormat;

/**
 *
 * @author acer
 */
public class TestCheckMaterialAndDetails {

    public static void main(String[] args) {
        CheckMatDao checkMatDao = new CheckMatDao();
//        
//        CheckMaterial chMat = checkMatDao.get(1);
//        System.out.println(chMat);
//        for (CheckMaterial c : checkMatDao.getAll()) {
//            System.out.println(c);
//        }
//
        EmployeeDao employeeDao = new EmployeeDao();
//
//        CheckMaterial chMat = new CheckMaterial(employeeDao.get(1));
//        checkMatDao.save(chMat);
//        System.out.println(chMat);
//
        Date date = new Date();
//
//        CheckMaterial chMatUp = checkMatDao.get(3);
//        System.out.println(chMatUp);
//        Employee employee = employeeDao.get(2);
//        chMatUp.setEmployee(employee);
//        checkMatDao.update(chMatUp);
//        CheckMaterial updateChMat = checkMatDao.get(3);
//        System.out.println(updateChMat);
//
//        checkMatDao.delete(checkMatDao.get(3));
//
//        for (CheckMaterial c : checkMatDao.getAll("check_mat_date='4/9/2022'", "employee_id DESC")) {
//            System.out.println(c);
//        }
//
//        for (CheckMaterial c : checkMatDao.getAll("employee_id DESC")) {
//            System.out.println(c);
//        }
//
        ChMatDetailDao chMatDetailDao = new ChMatDetailDao();
//
//        System.out.println(chMatDetailDao.get(1));
//        
//        for (ChMaterialDetail c : chMatDetailDao.getAll()) {
//            System.out.println(c);
//        }
//
//        for (ChMaterialDetail c : chMatDetailDao.getAll("check_mat_id = 1", "cmd_id ASC")) {
//            System.out.println(c);
//        }
//
//        for (ChMaterialDetail c : chMatDetailDao.getByChMaterialID(1)) {
//            System.out.println(c);
//        }
//
//        for (ChMaterialDetail c : chMatDetailDao.getAll("cmd_id DESC")) {
//            System.out.println(c);
//        }
//
        MaterialDao m = new MaterialDao();
        Material m1 = m.get(1);
        Material m2 = m.get(2);
        Material m3 = m.get(3);
        CheckMaterial chMat = new CheckMaterial(employeeDao.get(7));
//        chMat.addChMatDetail(m1, 9);
//        chMat.addChMatDetail(m2, 8);
//        chMat.addChMatDetail(m3, 7);
//        System.out.println(chMat);
//        System.out.println(chMat.getChMatDetails());
//        checkMatDao.save(chMat);
//        System.out.println(chMat);
//
//        ChMaterialDetail cmdUp = chMatDetailDao.get(25);
//        cmdUp.setQtyRemain(5);
//        chMatDetailDao.update(cmdUp);
//
//        checkMatDao.delete(checkMatDao.get(6));
//        chMatDetailDao.delete(chMatDetailDao.get(25));
//        chMatDetailDao.delete(chMatDetailDao.get(26));
//        chMatDetailDao.delete(chMatDetailDao.get(27));
//
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        System.out.println(sdf.format(date));
//        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
//        System.out.println(sdf1.format(date));
//        SimpleDateFormat sdfTime = new SimpleDateFormat("HH:mm:ss");
//        System.out.println(sdfTime.format(date));
//        
//        String d = FastDateFormat.getInstance(SQLiteConfig.DEFAULT_DATE_STRING_FORMAT).format(date);
//        System.out.println(d);
//        String d1 = FastDateFormat.getInstance("dd/MM/yyyy").format(date);
//        System.out.println(d1);
//        String d2 = FastDateFormat.getInstance("HH:mm").format(date);
//        System.out.println(d2);
    }
}
