/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package poc;

import com.mukku.dcoffee7.dao.GTCustRpDao;
import com.mukku.dcoffee7.model.GTimeCustReport;
import com.mukku.dcoffee7.service.GTimeRpService;
import java.util.List;

/**
 *
 * @author acer
 */
public class TestGTimeRp {

    public static void main(String[] args) {
//        GTCustRpDao gtcRpDao = new GTCustRpDao();
//        for (GTimeCustReport c : gtcRpDao.getGTimeReport()) {
//            System.out.println(c);
//        }
        GTimeRpService gtcRpService = new GTimeRpService();
        List<GTimeCustReport> gtcRp = gtcRpService.getGTimeCustReport();
        for (GTimeCustReport c : gtcRp) {
            System.out.println(c);
        }
    }
}
