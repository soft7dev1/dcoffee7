/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package poc;

import com.mukku.dcoffee7.dao.BillDao;
import com.mukku.dcoffee7.dao.BillDetailDao;
import com.mukku.dcoffee7.dao.EmployeeDao;
import com.mukku.dcoffee7.dao.MaterialDao;
import com.mukku.dcoffee7.model.Bill;
import com.mukku.dcoffee7.model.BillDetail;
import com.mukku.dcoffee7.model.Material;

/**
 *
 * @author acer
 */
public class TestBillMaterialAndDetails {

    public static void main(String[] args) {
        BillDao billDao = new BillDao();
//        System.out.println(billDao.get(1));
//        for (Bill b : billDao.getAll()) {
//            System.out.println(b);
//        }
//        for (Bill b : billDao.getAll("bill_total ASC")) {
//            System.out.println(b);
//        }
//        for (Bill b : billDao.getAll("bill_date='4/9/2022'", "employee_id DESC")) {
//            System.out.println(b);
//        }
//
        BillDetailDao billDetailDao = new BillDetailDao();
//        System.out.println(billDetailDao.get(1));
//        for (BillDetail c : billDetailDao.getAll()) {
//            System.out.println(c);
//        }
//        for (BillDetail c : billDetailDao.getAll("bill_detail_total DESC")) {
//            System.out.println(c);
//        }
//        for (BillDetail c : billDetailDao.getAll("bill_id=2", "bill_detail_amount ASC")) {
//            System.out.println(c);
//        }
//        for (BillDetail c : billDetailDao.getByBillMaterialID(1)) {
//            System.out.println(c);
//        }
//
        MaterialDao m = new MaterialDao();
        Material m1 = m.get(1);
        Material m2 = m.get(2);
        Material m3 = m.get(3);

        EmployeeDao employeeDao = new EmployeeDao();

//        Bill bill = new Bill("Cf_Mate", employeeDao.get(7));
//        bill.addBillMatDetail(m1, 9, 350);
//        bill.addBillMatDetail(m2, 8, 120);
//        bill.addBillMatDetail(m3, 7, 35);
//        System.out.println(bill);
//        System.out.println(bill.getBillMatDetails());
//        billDao.save(bill);
//        System.out.println(bill);
//
//        BillDetail billUp = billDetailDao.get(10);
//        billUp.setAmount(7);
//        billDetailDao.update(billUp);
//        Bill billUp2 = billDao.get(4);
//        billDao.update(billUp2);
//        Bill billUp = billDao.get(4);
//        billUp.setBuy(4000);
//        billDao.update(billUp);
//
        billDao.delete(billDao.get(4));
        billDetailDao.delete(billDetailDao.get(10));
        billDetailDao.delete(billDetailDao.get(11));
        billDetailDao.delete(billDetailDao.get(12));
    }
}
