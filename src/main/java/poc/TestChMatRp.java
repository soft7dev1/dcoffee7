/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package poc;

import com.mukku.dcoffee7.dao.ChMatRpDao;
import com.mukku.dcoffee7.model.ChMatReport;
import com.mukku.dcoffee7.service.ChMatRpService;
import java.util.List;

/**
 *
 * @author acer
 */
public class TestChMatRp {

    public static void main(String[] args) {
        ChMatRpDao chMatRpDao = new ChMatRpDao();
//        for (ChMatReport c : chMatRpDao.getNearlyOutReport(1)) {
//            System.out.println(c);
//        }
        ChMatRpService chMatRpService = new ChMatRpService();
        List<ChMatReport> chMatRp = chMatRpService.chMatReportById(1);
        for (ChMatReport c : chMatRp) {
            System.out.println(c);
        }
    }
}
