/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package poc;

import com.mukku.dcoffee7.dao.BillRpDao;
import com.mukku.dcoffee7.model.BillReport;
import com.mukku.dcoffee7.service.BillRpService;
import java.util.List;

/**
 *
 * @author acer
 */
public class TestBillRp {

    public static void main(String[] args) {
//        BillRpDao billRpDao = new BillRpDao();
//        for (BillReport b : billRpDao.getBillMonthReport(8)) {
//            System.out.println(b);
//        }

//         BillRpDao billRpDao = new BillRpDao();
//        for (BillReport b : billRpDao.getBillYearReport(2022)) {
//            System.out.println(b);
//        }
//        BillRpService billRpService = new BillRpService();
//        List<BillReport> billRp = billRpService.billReportByMonth(8);
//        for (BillReport b : billRp) {
//            System.out.println(b);
//        }
//        BillRpService billRpService = new BillRpService();
//        List<BillReport> billRp = billRpService.billReportByYear(2022);
//        for (BillReport b : billRp) {
//            System.out.println(b);
//        }

//        BillRpDao billRpDao = new BillRpDao();
//        for (String y : billRpDao.getYear()) {
//            System.out.println(y);
//        }
//        BillRpDao billRpDao = new BillRpDao();
//        for (String m : billRpDao.getMonth()) {
//            System.out.println(m);
//        }
//        BillRpService billRpService = new BillRpService();
//        List<String> billRp = billRpService.billReportGetYear();
//        for (String y : billRp) {
//            System.out.println(y);
//        }
        BillRpService billRpService = new BillRpService();
        List<String> billRp = billRpService.billReportGetMonth();
        for (String m : billRp) {
            System.out.println(m);
        }
    }
}
