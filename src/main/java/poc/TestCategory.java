/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poc;

import com.mukku.dcoffee7.dao.CategoryDao;
import com.mukku.dcoffee7.model.Category;

/**
 *
 * @author user
 */
public class TestCategory {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        CategoryDao catDao = new CategoryDao();
        System.out.println(catDao.get(1));
//        System.out.println(catDao.getAll("category_name like 'ก%' ", "category_id desc"));
//        Category newCat = new Category("แมวปั่นเพื่อสุขภาพ");
//        catDao.save(newCat);
        Category newCat = catDao.get(10);
        newCat.setName("แมวส้มปั่นเพื่อสุขภาพ");
        catDao.update(newCat);
        catDao.delete(newCat);

//        newCat.setName("เค้กแมวส้ม");
//        catDao.update(newCat);
        System.out.println(catDao.getAll("category_id desc"));
        for (Category pd : catDao.getAll()) {
            System.out.println(pd);
        }
    }

}
