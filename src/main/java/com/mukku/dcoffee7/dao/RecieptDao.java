/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mukku.dcoffee7.dao;

import com.mukku.dcoffee7.helper.DatabaseHelper;
import com.mukku.dcoffee7.model.Reciept;
import com.mukku.dcoffee7.model.RecieptDetail;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author acer
 */
public class RecieptDao implements Dao<Reciept>{

    @Override
    public Reciept get(int id) {
     Reciept reciept = null;
        String sql = "SELECT * FROM reciept WHERE rec_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                reciept = reciept.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return reciept;    
    }

    @Override
    public List<Reciept> getAll() {
          ArrayList<Reciept> list = new ArrayList();
        String sql = "SELECT * FROM reciept";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Reciept reciept = Reciept.fromRS(rs);
                list.add(reciept);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;  
    }

    @Override
    public Reciept save(Reciept obj) {
    RecieptDetailDao recieptDetailDao = new RecieptDetailDao();
        String sql = "INSERT INTO reciept ( rec_queue,rec_date,  rec_time,rec_discount,rec_total,rec_recieved,rec_change,rec_payment,store_id,employee_id,customer_id)"
                + "VALUES(?,?, ?,?, ?, ?, ?,?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
           // stmt.setInt(1, obj.getId());
                stmt.setInt(1, obj.getQueue());
            //stmt.setDate(3, (Date) obj.getDate());           
            //stmt.setDate(4, (Date) obj.getTime());
             stmt.setString(2, obj.getDateS());
            stmt.setString(3, obj.getTimeS());
            stmt.setDouble(4, obj.getDiscount());
            stmt.setDouble(5, obj.getTotal());
            stmt.setDouble(6, obj.getRecieved());
            stmt.setDouble(7, obj.getChange());
            stmt.setString(8, obj.getPaymant());
            stmt.setInt(9, obj.getSt_Id().getId());
            stmt.setInt(10, obj.getEm_Id().getId());
            stmt.setInt(11, obj.getCu_Id().getId());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
               //obj.setQueue(id); 
                obj.setId(id);
                for(RecieptDetail rd: obj.getRecieptDetail()){
                 RecieptDetail dt = recieptDetailDao.save(rd);
                    if(dt==null){
                        return null;
                    }
                }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;   
    }

    @Override
    public Reciept update(Reciept obj) {
      String sql = "UPDATE reciept"
                + " SET  rec_queue = ? , rec_date = ?, rec_time = ?,rec_discount = ?,rec_total = ?,rec_recieved = ?,rec_change=?,rec_payment = ?,store_id  = ?,employee_id = ?,customer_id = ?"
                + " WHERE rec_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
        //    stmt.setInt(1, obj.getId());
            stmt.setInt(1, obj.getQueue());
         //   stmt.setDate(2, (Date) obj.getDate());
         //   stmt.setDate(3, (Date) obj.getTime());
            stmt.setString(2, obj.getDateS());
            stmt.setString(3, obj.getTimeS());
            stmt.setDouble(4, obj.getDiscount());
            stmt.setDouble(5, obj.getTotal());
            stmt.setDouble(6, obj.getRecieved());
            stmt.setDouble(7, obj.getChange());
            stmt.setString(8, obj.getPaymant());
            stmt.setInt(9, obj.getSt_Id().getId());
            stmt.setInt(10, obj.getEm_Id().getId());
            stmt.setInt(11, obj.getCu_Id().getId());
            stmt.setInt(12,obj.getId());
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }  
    }

    @Override
    public int delete(Reciept obj) {
    String sql = "DELETE FROM reciept WHERE rec_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
      
    }

    @Override
    public List<Reciept> getAll(String where, String order) {
       ArrayList<Reciept> list = new ArrayList();
               String sql = "SELECT * FROM reciept  where " + where + " ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Reciept reciept = Reciept.fromRS(rs);
                list.add(reciept);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;    }
    
    
}
