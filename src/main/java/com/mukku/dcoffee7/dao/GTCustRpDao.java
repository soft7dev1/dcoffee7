/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mukku.dcoffee7.dao;

import com.mukku.dcoffee7.helper.DatabaseHelper;
import com.mukku.dcoffee7.model.GTimeCustReport;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author acer
 */
public class GTCustRpDao {

    public List<GTimeCustReport> getGTimeReport() {
        ArrayList<GTimeCustReport> list = new ArrayList();
        String sql = "SELECT (substr(rec_time, 0, instr(rec_time,':'))) || ':00 - ' || (substr(rec_time, 0, instr(rec_time,':'))) || ':59' AS time, COUNT(customer_id) AS num_people FROM golden_time_order\n"
                + "GROUP BY time\n"
                + "ORDER BY num_people DESC; ";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                GTimeCustReport gtc = GTimeCustReport.fromRS(rs);
                list.add(gtc);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
}
