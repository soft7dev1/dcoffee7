/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mukku.dcoffee7.dao;

import com.mukku.dcoffee7.helper.DatabaseHelper;
import com.mukku.dcoffee7.model.Customer;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JLabel;
import javax.swing.JTextField;

/**
 *
 * @author acer
 */
public class POSDao {

    public int findTel(JTextField edtFindTel) {
        try {
            String sql = "SELECT * FROM customer WHERE customer_tel=?";
            Connection conn = DatabaseHelper.getConnect();

            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, edtFindTel.getText());
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {

                if (rs.getString("customer_tel").equals(edtFindTel.getText())) {
                    return rs.getInt("customer_id");
                }
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    public int runQueue(JLabel txtDate) {
        int i = 0;
        try {
            String sql = "SELECT * FROM reciept WHERE rec_date=?";
            Connection conn = DatabaseHelper.getConnect();

            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, txtDate.getText());
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {

                if (rs.getString("rec_date").equals(txtDate.getText())) {
                    i++;
                }

            }
            return i + 1;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return 1;
    }
}
