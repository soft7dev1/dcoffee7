/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mukku.dcoffee7.dao;

import com.mukku.dcoffee7.helper.DatabaseHelper;
import com.mukku.dcoffee7.model.BillReport;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author acer
 */
public class BillRpDao {

    public List<BillReport> getBillMonthReport(int month) {
        ArrayList<BillReport> list = new ArrayList();
        String sql = "SELECT substr(bill_date, instr(bill_date,'/')+1, length(bill_date)) AS month, mat_id, bill_detail_name, SUM(bill_detail_amount) AS amount, SUM(bill_detail_total) AS total  FROM import_material\n"
                + "WHERE  rtrim(ltrim(substr(bill_date, 4, instr(bill_date,'/')), \"0\"), \"/\")=\"" + month + "\"\n"
                + "GROUP BY mat_id\n"
                + "ORDER BY amount DESC, total DESC;";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                BillReport bill = BillReport.fromRS(rs);
                list.add(bill);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<BillReport> getBillYearReport(int year) {
        ArrayList<BillReport> list = new ArrayList();
        String sql = "SELECT substr(bill_date, instr(bill_date,'/')+4, length(bill_date)) AS year, mat_id, bill_detail_name, SUM(bill_detail_amount) AS amount, SUM(bill_detail_total) AS total FROM import_material\n"
                + "WHERE  year=\"" + year + "\"\n"
                + "GROUP BY mat_id\n"
                + "ORDER BY amount DESC, total DESC;";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                BillReport bill = BillReport.fromRS(rs);
                list.add(bill);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<String> getYear() {
        ArrayList<String> list = new ArrayList();
        String sql = "SELECT DISTINCT substr(bill_date, instr(bill_date,'/')+4, length(bill_date)) AS year FROM import_material\n"
                + "ORDER BY year DESC;";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                list.add(rs.getString("year"));
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<String> getMonth() {
        ArrayList<String> list = new ArrayList();
        String sql = "SELECT DISTINCT substr(bill_date, instr(bill_date,'/')+1, length(bill_date)) AS month FROM import_material\n"
                + "ORDER BY month DESC;";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                list.add(rs.getString("month"));
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
}
