/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mukku.dcoffee7.dao;

import com.mukku.dcoffee7.helper.DatabaseHelper;
import com.mukku.dcoffee7.model.RecieptDetail;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author acer
 */
public class RecieptDetailDao implements Dao<RecieptDetail>{

    @Override
    public RecieptDetail get(int id) {
    RecieptDetail item = null;
    String sql ="SELECT * FROM  reciept_detail WHERE  rcd_id=?";
  Connection conn =DatabaseHelper.getConnect();
     try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                item = RecieptDetail.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return item;   
    }

    @Override
    public List<RecieptDetail> getAll() {
        ArrayList<RecieptDetail> list = new ArrayList();
        String sql = "SELECT * FROM reciept_detail";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                RecieptDetail item = RecieptDetail.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;  
    }

    @Override
    public RecieptDetail save(RecieptDetail obj) {
       
        String sql = "INSERT INTO reciept_detail (product_id,rcd_name,rcd_amount,rcd_price,rcd_total,rec_id)"
                + "VALUES(?, ?, ?, ?, ?,?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
           
            
            stmt.setString(2, obj.getProduct().getName());
            stmt.setDouble(3, obj.getAmount());
            stmt.setDouble(4, obj.getProduct().getPrice());
            stmt.setDouble(5, obj.getTotal()); 
            stmt.setInt(1, obj.getProduct().getId());
            stmt.setInt(6, obj.getReciept().getId());
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj; 
    }

    @Override
    public RecieptDetail update(RecieptDetail obj) {
           String sql = "UPDATE reciept_detail"
                + " SET rcd_name = ?, rcd_amount = ?, rcd_price = ?, rcd_total = ?, product_id = ?"
                + " WHERE rcd_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setInt(2, obj.getAmount());
            stmt.setDouble(3, obj.getPrice());
            stmt.setDouble(4, obj.getTotal());
            stmt.setInt(5, obj.getProduct().getId());
          //  stmt.setInt(6, obj.getReciept().getId());
            stmt.setInt(6,obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    
    }

    @Override
    public int delete(RecieptDetail obj) {
       String sql = "DELETE FROM reciept_detail WHERE rcd_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;        
    }

    @Override
    public List<RecieptDetail> getAll(String where, String reciept) {
        ArrayList<RecieptDetail> list = new ArrayList();
        String sql = "SELECT * FROM reciept_detail where " + where + " ORDER BY " + reciept;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                RecieptDetail item = RecieptDetail.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;  
    }
     public List<RecieptDetail> getbyrecieptID(int recid) {
        return getAll("rec_id = " + recid, "rcd_id ASC");
       }
}
