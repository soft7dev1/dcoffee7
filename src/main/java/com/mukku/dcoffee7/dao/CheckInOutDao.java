/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mukku.dcoffee7.dao;

import com.mukku.dcoffee7.helper.DatabaseHelper;
import com.mukku.dcoffee7.model.CheckInOut;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Acer
 */
public class CheckInOutDao implements Dao<CheckInOut> {

    @Override
    public CheckInOut get(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<CheckInOut> getAll() {
        ArrayList<CheckInOut> list = new ArrayList();
        String sql = "SELECT * FROM check_in_out";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckInOut item = CheckInOut.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public CheckInOut save(CheckInOut obj) {
        String sql = "INSERT INTO check_in_out(employee_id)\n"
                + "VALUES(?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getEmpId());

            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public CheckInOut update(CheckInOut obj) {
        String sql = "UPDATE check_in_out\n"
                + "SET cio_time_out = strftime('%H:%M',datetime('now', 'localtime')),\n"
                + "cio_total_hour = strftime('%H:%M',time((strftime('%s',datetime('now', 'localtime'))-strftime('%s',cio_time_in))/86400.0),'-12 hours')\n"
                + "WHERE check_in_out.cio_date = strftime('%d/%m/%Y',datetime('now', 'localtime'))\n"
                + "AND EXISTS (SELECT employee_id FROM employee WHERE employee_id = (check_in_out.employee_id=?))\n";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getEmpId());

            int ret = stmt.executeUpdate();
            //System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(CheckInOut obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<CheckInOut> getAll(String where, String order) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public CheckInOut getInOutTime(int emp_id) {
        CheckInOut item = null;
        String sql = "SELECT * FROM check_in_out WHERE check_in_out.cio_date = strftime('%d/%m/%Y',DATE('now', 'localtime'))\n"
                + "AND EXISTS (SELECT employee_id FROM employee WHERE employee_id = (check_in_out.employee_id=?));";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, emp_id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                item = CheckInOut.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return item;
    }

    public CheckInOut updateSsId(CheckInOut obj, int emp_id) {
        String sql = "update check_in_out\n"
                + "set ss_id = ?\n"
                + "where employee_id = " + emp_id + " AND ss_id is null";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getSsId());

            int ret = stmt.executeUpdate();
            //System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }
    
    public CheckInOut updateNullSsId(CheckInOut obj) {
        String sql = "update check_in_out\n"
                + "set ss_id = null\n"
                + "where ss_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getSsId());

            int ret = stmt.executeUpdate();
            //System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }
}
