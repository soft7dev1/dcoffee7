/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mukku.dcoffee7.dao;

import com.mukku.dcoffee7.helper.DatabaseHelper;
import com.mukku.dcoffee7.model.PopProductRp;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author user
 */
public class PopProdRpDao {

    public List<PopProductRp> getPopProductReport() {
        ArrayList<PopProductRp> list = new ArrayList();
        String sql = "SELECT rcd_name AS name,count(product_id) AS pop_product FROM popular_product \n"
                + "GROUP BY rcd_name \n"
                + "ORDER BY pop_product DESC LIMIT 0,5";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                PopProductRp popproduct = PopProductRp.fromRS(rs);
                list.add(popproduct);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

}
