/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mukku.dcoffee7.dao;

import com.mukku.dcoffee7.helper.DatabaseHelper;
import com.mukku.dcoffee7.model.ChMatReport;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author acer
 */
public class ChMatRpDao {

    public List<ChMatReport> getNearlyOutReport(int chMatId) {
        ArrayList<ChMatReport> list = new ArrayList();
        String sql = "SELECT cmd_name, mat_min_quantity, cmd_qty_remain  FROM nearly_out_of_material\n"
                + "WHERE check_mat_id = " + chMatId + " and mat_min_quantity > cmd_qty_remain; ";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ChMatReport chMat = ChMatReport.fromRS(rs);
                list.add(chMat);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
}
