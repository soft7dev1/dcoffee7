/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mukku.dcoffee7.dao;

import com.mukku.dcoffee7.helper.DatabaseHelper;
import com.mukku.dcoffee7.model.Bill;
import com.mukku.dcoffee7.model.BillDetail;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author acer
 */
public class BillDao implements Dao<Bill> {

    private BillDetailDao billMaterialDDao;

    @Override
    public Bill get(int id) {
        Bill billMaterial = null;
        String sql = "SELECT * FROM bill WHERE bill_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                billMaterial = Bill.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return billMaterial;
    }

    @Override
    public List<Bill> getAll() {
        ArrayList<Bill> list = new ArrayList();
        String sql = "SELECT * FROM bill";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Bill billMaterial = Bill.fromRS(rs);
                list.add(billMaterial);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<Bill> getAll(String where, String order) {
        ArrayList<Bill> list = new ArrayList();
        String sql = "SELECT * FROM bill where " + where + " ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Bill item = Bill.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<Bill> getAll(String order) {
        ArrayList<Bill> list = new ArrayList();
        String sql = "SELECT * FROM bill ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Bill item = Bill.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Bill save(Bill obj) {
        billMaterialDDao = new BillDetailDao();
        String sql = "INSERT INTO bill (employee_id, bill_shop_name, bill_total, bill_buy, bill_change, bill_date, bill_time)"
                + "VALUES(?, ?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getEmployee().getId());
            stmt.setString(2, obj.getShopName());
            stmt.setDouble(3, obj.getTotal());
            stmt.setDouble(4, obj.getBuy());
            stmt.setDouble(5, obj.getChange());
            stmt.setString(6, obj.getDateS());
            stmt.setString(7, obj.getTimeS());
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            //add detail
//            obj.setId(id);
//            for (BillDetail bd : obj.getBillMatDetails()) {
//                BillDetail dt = billMaterialDDao.save(bd);
//                if (dt == null) {
//                    return null;
//                }
//            }
            return get(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public Bill update(Bill obj) {
        String sql = "UPDATE bill"
                + " SET employee_id = ?, bill_shop_name = ?, bill_total = ?, bill_buy = ?, bill_change = ?, bill_date = ?, bill_time = ?"
                + " WHERE bill_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getEmployee().getId());
            stmt.setString(2, obj.getShopName());
//            double total = 0;
//            BillDetailDao bdd = new BillDetailDao();
//            for (BillDetail bd : bdd.getByBillMaterialID(obj.getId())) {
//                System.out.println(bd.getTotal());
//                total += bd.getTotal();
//            }
//            stmt.setDouble(3, total);
            stmt.setDouble(3, obj.getTotal());
            stmt.setDouble(4, obj.getBuy());
            stmt.setDouble(5, obj.getChange());
            stmt.setString(6, obj.getDateS());
            stmt.setString(7, obj.getTimeS());
            stmt.setInt(8, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Bill obj) {
        String sql = "DELETE FROM bill WHERE bill_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

}
