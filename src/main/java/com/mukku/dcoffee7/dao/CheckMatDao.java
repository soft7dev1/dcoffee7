/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mukku.dcoffee7.dao;

import com.mukku.dcoffee7.helper.DatabaseHelper;
import com.mukku.dcoffee7.model.ChMaterialDetail;
import com.mukku.dcoffee7.model.CheckMaterial;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author acer
 */
public class CheckMatDao implements Dao<CheckMaterial> {

    ChMatDetailDao chMatDetailDao = new ChMatDetailDao();

    @Override
    public CheckMaterial get(int id) {
        CheckMaterial chMaterial = null;
        String sql = "SELECT * FROM check_material WHERE check_mat_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                chMaterial = CheckMaterial.fromRS(rs);
                List<ChMaterialDetail> chMatDetails = chMatDetailDao.getByChMaterialID(chMaterial.getId());
                chMaterial.setChMatDetails((ArrayList<ChMaterialDetail>) chMatDetails);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return chMaterial;
    }

    @Override
    public List<CheckMaterial> getAll() {
        ArrayList<CheckMaterial> list = new ArrayList();
        String sql = "SELECT * FROM check_material";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckMaterial chMaterial = CheckMaterial.fromRS(rs);
                list.add(chMaterial);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<CheckMaterial> getAll(String where, String order) {
        ArrayList<CheckMaterial> list = new ArrayList();
        String sql = "SELECT * FROM check_material where " + where + " ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckMaterial item = CheckMaterial.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<CheckMaterial> getAll(String order) {
        ArrayList<CheckMaterial> list = new ArrayList();
        String sql = "SELECT * FROM check_material ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckMaterial item = CheckMaterial.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public CheckMaterial save(CheckMaterial obj) {
        String sql = "INSERT INTO check_material (employee_id, check_mat_date, check_mat_time)"
                + "VALUES(?, ?,?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getEmployee().getId());
//            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
//            stmt.setString(2, sdf.format(obj.getDate()));
//            SimpleDateFormat sdf2 = new SimpleDateFormat("HH:mm");
//            stmt.setString(3, sdf2.format(obj.getTime()));
//            stmt.setString(2, FastDateFormat.getInstance("dd/MM/yyyy").format(obj.getDate()));
//            stmt.setString(3, FastDateFormat.getInstance("HH:mm").format(obj.getTime()));
            stmt.setString(2, obj.getDateS());
            stmt.setString(3, obj.getTimeS());
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
            for (ChMaterialDetail cmd : obj.getChMatDetails()) {
                ChMaterialDetail dt = chMatDetailDao.save(cmd);
                if (dt == null) {
                    DatabaseHelper.endTransactionWithRollBack();
                    return null;
                }
            }
            return get(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public CheckMaterial update(CheckMaterial obj) {
        String sql = "UPDATE check_material"
                + " SET employee_id = ?, check_mat_date = ?, check_mat_time = ?"
                + " WHERE check_mat_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getEmployee().getId());
            stmt.setString(2, obj.getDateS());
            stmt.setString(3, obj.getTimeS());
            stmt.setInt(4, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(CheckMaterial obj) {
        String sql = "DELETE FROM check_material WHERE check_mat_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }
}
