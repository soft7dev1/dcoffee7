/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mukku.dcoffee7.dao;

import com.mukku.dcoffee7.helper.DatabaseHelper;
import com.mukku.dcoffee7.model.MTypeCusReport;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ทักช์ติโชค
 */
public class MTCusRpDao {

    public List<MTypeCusReport> getTypeCusReport() {
        ArrayList<MTypeCusReport> list = new ArrayList();
        String sql = "SELECT substr(is_member_num.rec_date, instr(is_member_num.rec_date,'/')+1, length(is_member_num.rec_date)) AS month, is_member_num.is_member,is_not_member_num.is_not_member\n"
                + "FROM is_member_num\n"
                + "LEFT JOIN is_not_member_num \n"
                + "ON is_member_num.rec_date = is_not_member_num.rec_date\n"
                + "UNION  \n"
                + "SELECT substr(is_not_member_num.rec_date, instr(is_not_member_num.rec_date,'/')+1, length(is_not_member_num.rec_date)) AS month, is_member_num.is_member,is_not_member_num.is_not_member\n"
                + "FROM is_not_member_num\n"
                + "LEFT JOIN is_member_num \n"
                + "ON is_not_member_num.rec_date = is_member_num.rec_date\n"
                + "GROUP BY is_member_num.rec_date; ";

        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                MTypeCusReport mty = MTypeCusReport.fromRS(rs);
                list.add(mty);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

}
