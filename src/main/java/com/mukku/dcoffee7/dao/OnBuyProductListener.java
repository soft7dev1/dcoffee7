/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.mukku.dcoffee7.dao;

import com.mukku.dcoffee7.model.Product;
import com.mukku.dcoffee7.model.Reciept;

/**
 *
 * @author acer
 */
public interface OnBuyProductListener {
    public void Buy(Product product, String prodName, int amount, double price);
}
