/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mukku.dcoffee7.dao;

import com.mukku.dcoffee7.helper.DatabaseHelper;
import com.mukku.dcoffee7.model.EmpSalaryReport;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author acer
 */
//edit 
public class EmpSalaryReportDao {

    public List<EmpSalaryReport> getempsDayReports(String month) {
        ArrayList<EmpSalaryReport> list = new ArrayList();
        String sql = "SELECT substr(ss_date,0,instr(ss_date,' ')) AS day, SUM(ss_salary) AS total FROM summary_salary\n"
                + "WHERE substr(substr(ss_date,0,instr(ss_date,' ')), instr(ss_date,'/')+1)=\"" + month + "\"\n"
                + "GROUP BY day ORDER BY day DESC;";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                EmpSalaryReport emps = EmpSalaryReport.formRSDay(rs);
                list.add(emps);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;

    }

    public List<EmpSalaryReport> getempsMonthReports(int year) {
        ArrayList<EmpSalaryReport> list = new ArrayList();
        String sql = "SELECT substr(substr(ss_date,0,instr(ss_date,' ')) ,instr(ss_date,'/')+1) AS month, SUM(ss_salary) AS total FROM summary_salary\n"
                + "WHERE substr(substr(ss_date,0,instr(ss_date,' ')), instr(ss_date,'/')+4)=\"" + year + "\"\n"
                + "GROUP BY month ORDER BY month DESC;";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                EmpSalaryReport emps = EmpSalaryReport.formRSMonth(rs);
                list.add(emps);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;

    }

    public List<EmpSalaryReport> getempsYearReports() {
        ArrayList<EmpSalaryReport> list = new ArrayList();
        String sql = "SELECT substr(substr(ss_date, 0 ,instr(ss_date,' ')), instr(ss_date,'/')+4) AS year, SUM(ss_salary) AS total FROM summary_salary\n"
                + "GROUP BY year\n"
                + "ORDER BY year DESC; ";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                EmpSalaryReport emps = EmpSalaryReport.formRSYear(rs);
                list.add(emps);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;

    }

//    public List<String> getDay() {
//        ArrayList<String> list = new ArrayList();
//        String sql = "SELECT DISTINCT  substr(ss_date,0, instr(ss_date,' ')) AS day FROM summary_salary \n"
//                + "WHERE substr(ss_date, instr(ss_date,'/')+1, length(ss_date))\n"
//                + "GROUP BY day\n"
//                + "ORDER BY day DESC;";
//        Connection conn = DatabaseHelper.getConnect();
//        try {
//            Statement stmt = conn.createStatement();
//            ResultSet rs = stmt.executeQuery(sql);
//
//            while (rs.next()) {
//                list.add(rs.getString("day"));
//            }
//
//        } catch (SQLException ex) {
//            System.out.println(ex.getMessage());
//        }
//        return list;
//    }
    public List<String> getMonth() {
        ArrayList<String> list = new ArrayList();
        String sql = "SELECT DISTINCT substr(substr(ss_date,0,instr(ss_date,' ')), instr(ss_date,'/')+1) AS month FROM summary_salary;";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                list.add(rs.getString("month"));
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<String> getYear() {
        ArrayList<String> list = new ArrayList();
        String sql = "SELECT DISTINCT substr(substr(ss_date,0,instr(ss_date,' ')), instr(ss_date,'/')+4) AS year FROM summary_salary";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                list.add(rs.getString("year"));
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
}
