/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mukku.dcoffee7.dao;

import com.mukku.dcoffee7.helper.DatabaseHelper;
import com.mukku.dcoffee7.model.SaleReport;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Acer
 */
public class SaleReportDao {

    public List<SaleReport> getDayReport(String month) {
        ArrayList<SaleReport> list = new ArrayList();
        String sql = "SELECT  rec_date AS day  ,SUM(rec_total) AS total  FROM reciept\n"
                + "WHERE substr(rec_date, instr(rec_date,'/')+1, length(rec_date)) = \"" + month + "\"\n"
                + "GROUP BY day\n"
                + "ORDER BY day DESC";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                SaleReport item = SaleReport.fromRSDay(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<SaleReport> getMonthReport(int year) {
        ArrayList<SaleReport> list = new ArrayList();
        String sql = "SELECT substr(rec_date, instr(rec_date,'/')+1, length(rec_date)) AS month, SUM(rec_total) AS total FROM reciept\n"
                + "WHERE substr(rec_date, instr(rec_date,'/')+4, length(rec_date))=\"" + year + "\"\n"
                + "GROUP BY month\n"
                + "ORDER BY month DESC";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                SaleReport item = SaleReport.fromRSMonth(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<SaleReport> getYearReport() {
        ArrayList<SaleReport> list = new ArrayList();
        String sql = "SELECT substr(rec_date, instr(rec_date,'/')+4, length(rec_date)) AS year, SUM(rec_total) AS total FROM reciept\n"
                + "GROUP BY year\n"
                + "ORDER BY year DESC";

        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                SaleReport item = SaleReport.fromRSYear(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<String> getDay() {
        ArrayList<String> list = new ArrayList();
        String sql = "SELECT substr(rec_date, instr(rec_date,'/')+1, length(rec_date))  AS day , SUM(rec_total) AS total FROM reciept \n"
                + "GROUP BY day\n"
                + "ORDER BY day DESC;";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                list.add(rs.getString("day"));
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<String> getMonth() {
        ArrayList<String> list = new ArrayList();
        String sql = "SELECT substr(rec_date, instr(rec_date,'/')+4, length(rec_date)) AS month, SUM(rec_total) AS total FROM reciept\n"
                + "GROUP BY month\n"
                + "ORDER BY month DESC;";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                list.add(rs.getString("month"));
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<String> getYear() {
        ArrayList<String> list = new ArrayList();
        String sql = "SELECT substr(rec_date, instr(rec_date,'/')+4, length(rec_date)) AS year, SUM(rec_total) AS total FROM reciept GROUP BY year ORDER BY year DESC;";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                list.add(rs.getString("year"));
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
}
