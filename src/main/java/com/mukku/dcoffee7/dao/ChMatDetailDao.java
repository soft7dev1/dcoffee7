/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mukku.dcoffee7.dao;

import com.mukku.dcoffee7.helper.DatabaseHelper;
import com.mukku.dcoffee7.model.ChMaterialDetail;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author acer
 */
public class ChMatDetailDao implements Dao<ChMaterialDetail> {

    @Override
    public ChMaterialDetail get(int id) {
        ChMaterialDetail item = null;
        String sql = "SELECT * FROM check_material_detail WHERE cmd_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                item = ChMaterialDetail.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return item;
    }

    @Override
    public List<ChMaterialDetail> getAll() {
        ArrayList<ChMaterialDetail> list = new ArrayList();
        String sql = "SELECT * FROM check_material_detail";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ChMaterialDetail item = ChMaterialDetail.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<ChMaterialDetail> getAll(String where, String order) {
        ArrayList<ChMaterialDetail> list = new ArrayList();
        String sql = "SELECT * FROM check_material_detail where " + where + " ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ChMaterialDetail item = ChMaterialDetail.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<ChMaterialDetail> getByChMaterialID(int chMatId) {
        return getAll("check_mat_id = " + chMatId, "cmd_id ASC");
    }

    public List<ChMaterialDetail> getAll(String order) {
        ArrayList<ChMaterialDetail> list = new ArrayList();
        String sql = "SELECT * FROM check_material_detail ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ChMaterialDetail item = ChMaterialDetail.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public ChMaterialDetail save(ChMaterialDetail obj) {
        String sql = "INSERT INTO check_material_detail (mat_id, cmd_name, cmd_qty_last, cmd_qty_remain, check_mat_id)"
                + "VALUES(?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getMaterial().getId());
//            stmt.setString(2, obj.getMatName());
//            stmt.setInt(3, obj.getQtyLast());
            stmt.setString(2, obj.getMaterial().getName());
            stmt.setInt(3, obj.getMaterial().getQuantity());
            stmt.setInt(4, obj.getQtyRemain());
            stmt.setInt(5, obj.getChMaterial().getId());
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public ChMaterialDetail update(ChMaterialDetail obj) {
        String sql = "UPDATE check_material_detail"
                + " SET cmd_name = ?, cmd_qty_last = ?, cmd_qty_remain = ?, mat_id = ?"
                + " WHERE cmd_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getMatName());
            stmt.setInt(2, obj.getQtyLast());
            stmt.setInt(3, obj.getQtyRemain());
            stmt.setInt(4, obj.getMaterial().getId());
            stmt.setInt(5, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(ChMaterialDetail obj) {
        String sql = "DELETE FROM check_material_detail WHERE cmd_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }
}
