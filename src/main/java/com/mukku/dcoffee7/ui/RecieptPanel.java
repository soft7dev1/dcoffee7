/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package com.mukku.dcoffee7.ui;

import com.mukku.dcoffee7.model.Reciept;
import com.mukku.dcoffee7.service.RecieptService;
import java.util.Date;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;
import com.mukku.dcoffee7.model.Reciept;
import org.sqlite.date.FastDateFormat;
import com.mukku.dcoffee7.service.RecieptService;

public class RecieptPanel extends javax.swing.JPanel {
    
    private final RecieptService reciptService;
    private List<Reciept> reciepList;
    private Reciept editedRectiept;
    private MainFrame mainFrame;
    
    public RecieptPanel() {
        initComponents();
        reciptService = new RecieptService();
        reciepList = reciptService.getReciept();
        tblReciept.setModel(new AbstractTableModel() {
            @Override
            public String getColumnName(int column) {
                String[] columnName = {"ID", "Queue", "Date", "Time", "Discount", "Total", "Recieve", "Change", "Payment", "Store ID", "Empolyee ID", "Customer ID"};
                return columnName[column];
            }
            
            @Override
            public int getRowCount() {
                return reciepList.size();
            }
            
            @Override
            public int getColumnCount() {
                return 12;
            }
            
            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                Reciept reciept = reciepList.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return reciept.getId();
                    case 1:
                        return reciept.getQueue();
                    case 2:
                        return reciept.getDateS();
                    case 3:
                        return reciept.getTimeS();
                    case 4:
                        return reciept.getDiscount();
                    case 5:
                        return reciept.getTotal();
                    case 6:
                        return reciept.getRecieved();
                    case 7:
                        return reciept.getChange();
                    case 8:
                        return reciept.getPaymant();
                    case 9:
                        return reciept.getSt_Id().getId();
                    case 10:
                        return reciept.getEm_Id().getId();
                    case 11:
                        return reciept.getCu_Id().getId();
                    default:
                        return "Unknown";
                }
            }
            
        });
        enableForm(false);
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        edtqueue = new javax.swing.JTextField();
        edtDate = new javax.swing.JTextField();
        edtTime = new javax.swing.JTextField();
        edtDiscount = new javax.swing.JTextField();
        edtTotal = new javax.swing.JTextField();
        edtRecieve = new javax.swing.JTextField();
        edtChange = new javax.swing.JTextField();
        btnsave = new javax.swing.JButton();
        btnclear = new javax.swing.JButton();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        edtstid = new javax.swing.JTextField();
        edtempid = new javax.swing.JTextField();
        edtcusid = new javax.swing.JTextField();
        cmbPayment = new javax.swing.JComboBox<>();
        txtId = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblReciept = new javax.swing.JTable();
        jPanel2 = new javax.swing.JPanel();
        btnadd = new javax.swing.JButton();
        btnEdit = new javax.swing.JButton();
        btndelete = new javax.swing.JButton();
        btnmore = new javax.swing.JButton();

        jPanel1.setBackground(new java.awt.Color(228, 209, 161));

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel1.setText("ID:");

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel2.setText("Queue:");

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel3.setText("Date:");

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel4.setText("Time:");

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel5.setText("PayTime");

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel6.setText("Discount :");

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel7.setText("Total :");

        jLabel8.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel8.setText("Recieve :");

        jLabel9.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel9.setText("Change :");

        edtqueue.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        edtDate.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        edtTime.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        edtDiscount.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        edtTotal.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        edtRecieve.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        edtChange.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        btnsave.setBackground(new java.awt.Color(255, 255, 255));
        btnsave.setText("Save");
        btnsave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsaveActionPerformed(evt);
            }
        });

        btnclear.setBackground(new java.awt.Color(255, 255, 255));
        btnclear.setText("Clear");
        btnclear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnclearActionPerformed(evt);
            }
        });

        jLabel10.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel10.setText("Store ID :");

        jLabel11.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel11.setText("Employee ID :");

        jLabel12.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel12.setText("Customer ID :");

        edtstid.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        edtempid.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        edtcusid.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        cmbPayment.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        cmbPayment.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "P", "C" }));

        txtId.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel3)
                            .addComponent(jLabel4)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(txtId, javax.swing.GroupLayout.DEFAULT_SIZE, 28, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel2))))
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(edtqueue, javax.swing.GroupLayout.DEFAULT_SIZE, 137, Short.MAX_VALUE)
                    .addComponent(edtDate)
                    .addComponent(edtTime)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(cmbPayment, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(29, 29, 29)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel6)
                            .addComponent(jLabel7))
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(14, 14, 14)
                                .addComponent(edtTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addComponent(edtDiscount, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(32, 32, 32)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel10)
                                .addGap(39, 39, 39)
                                .addComponent(edtstid))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel11)
                                .addGap(18, 18, 18)
                                .addComponent(edtempid))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel12)
                                .addGap(18, 18, 18)
                                .addComponent(edtcusid, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 172, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel9)
                                .addGap(18, 18, 18)
                                .addComponent(edtChange))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel8)
                                .addGap(18, 18, 18)
                                .addComponent(edtRecieve, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnsave)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnclear)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2)
                    .addComponent(jLabel6)
                    .addComponent(edtqueue, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(edtDiscount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10)
                    .addComponent(edtstid, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtId, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jLabel7)
                    .addComponent(edtDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(edtTotal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel11)
                    .addComponent(edtempid, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jLabel8)
                    .addComponent(edtTime, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(edtRecieve, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel12)
                    .addComponent(edtcusid, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel9)
                                .addComponent(edtChange, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel5)
                                .addComponent(cmbPayment, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 24, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnsave)
                            .addComponent(btnclear))
                        .addGap(23, 23, 23))))
        );

        tblReciept.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        tblReciept.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4", "Title 5", "Title 6", "Title 7", "Title 8", "Title 9", "Title 10", "Title 11", "Title 12", "Title 13"
            }
        ));
        jScrollPane1.setViewportView(tblReciept);

        jPanel2.setBackground(new java.awt.Color(228, 209, 161));

        btnadd.setBackground(new java.awt.Color(255, 255, 255));
        btnadd.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btnadd.setText("Add");
        btnadd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnaddActionPerformed(evt);
            }
        });

        btnEdit.setBackground(new java.awt.Color(255, 255, 255));
        btnEdit.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btnEdit.setText("Edit");
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });

        btndelete.setBackground(new java.awt.Color(255, 255, 255));
        btndelete.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btndelete.setText("Delete");
        btndelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btndeleteActionPerformed(evt);
            }
        });

        btnmore.setBackground(new java.awt.Color(255, 255, 255));
        btnmore.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btnmore.setText("More Detail");
        btnmore.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnmoreActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnadd)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnEdit)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btndelete)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnmore)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnadd)
                    .addComponent(btnEdit)
                    .addComponent(btndelete)
                    .addComponent(btnmore))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 282, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnsaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsaveActionPerformed
        
        setFormToObj();
        if (editedRectiept.getId() < 0) {
            reciptService.addReciept(editedRectiept);
        } else {
            reciptService.updateReciept(editedRectiept);
        }
        enableForm(false);
        refreshTable();
    }//GEN-LAST:event_btnsaveActionPerformed

    private void btnclearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnclearActionPerformed
        enableForm(false);
        editedRectiept = null;
    }//GEN-LAST:event_btnclearActionPerformed

    private void btndeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btndeleteActionPerformed
        int selectIndex = tblReciept.getSelectedRow();
        if (selectIndex >= 0) {
            editedRectiept = reciepList.get(selectIndex);
            int input = JOptionPane.showConfirmDialog(this, "Do you want to proceed?", "Select an Option", JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE);
            if (input == 0) {
                reciptService.deleteReciept(editedRectiept);
            }
            refreshTable();
        }
    }//GEN-LAST:event_btndeleteActionPerformed

    private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
        int selectindex = tblReciept.getSelectedRow();
        if (selectindex >= 0) {
            editedRectiept = reciepList.get(selectindex);
            setObjToFormEdit();
            enableForm(true);
        }
    }//GEN-LAST:event_btnEditActionPerformed

    private void btnaddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnaddActionPerformed
        editedRectiept = new Reciept();
        setObjectToForm();
        enableForm(true);
    }//GEN-LAST:event_btnaddActionPerformed

    private void btnmoreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnmoreActionPerformed
        int selectIndex = tblReciept.getSelectedRow();
        if (selectIndex >= 0) {
            editedRectiept = reciepList.get(selectIndex);
            mainFrame = new MainFrame();
            mainFrame.setVisible(true);
            mainFrame.showRecieptDetail(this, editedRectiept.getId());
        }
        editedRectiept = null;
    }//GEN-LAST:event_btnmoreActionPerformed
    private void enableForm(boolean stt) {
        if (stt == false) {
            edtChange.setText("");
            edtDate.setText("");
            edtDiscount.setText("");
            cmbPayment.setSelectedIndex(0);
            edtRecieve.setText("");
            edtTime.setText("");
            edtTotal.setText("");
            edtqueue.setText("");
            edtstid.setText("");
            edtempid.setText("");
            edtcusid.setText("");
        }
        edtChange.setEnabled(stt);
        edtDate.setEnabled(stt);
        edtDiscount.setEnabled(stt);
        cmbPayment.setEnabled(stt);
        edtRecieve.setEnabled(stt);
        edtTime.setEnabled(stt);
        edtTotal.setEnabled(stt);
        edtqueue.setEnabled(stt);
        edtstid.setEnabled(stt);
        edtempid.setEnabled(stt);
        edtcusid.setEnabled(stt);
        btnsave.setEnabled(stt);
        btnclear.setEnabled(stt);
        edtstid.requestFocus();
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnEdit;
    private javax.swing.JButton btnadd;
    private javax.swing.JButton btnclear;
    private javax.swing.JButton btndelete;
    private javax.swing.JButton btnmore;
    private javax.swing.JButton btnsave;
    private javax.swing.JComboBox<String> cmbPayment;
    private javax.swing.JTextField edtChange;
    private javax.swing.JTextField edtDate;
    private javax.swing.JTextField edtDiscount;
    private javax.swing.JTextField edtRecieve;
    private javax.swing.JTextField edtTime;
    private javax.swing.JTextField edtTotal;
    private javax.swing.JTextField edtcusid;
    private javax.swing.JTextField edtempid;
    private javax.swing.JTextField edtqueue;
    private javax.swing.JTextField edtstid;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tblReciept;
    private javax.swing.JLabel txtId;
    // End of variables declaration//GEN-END:variables

    private void setObjectToForm() {
        Date date = new Date();
        txtId.setText("" + editedRectiept.getId());
        edtDate.setText(FastDateFormat.getInstance("dd/MM/yyyy").format(date));
        edtTime.setText(FastDateFormat.getInstance("HH:mm").format(date));
        edtDiscount.setText("" + editedRectiept.getDiscount());
        edtChange.setText("" + editedRectiept.getChange());
        edtRecieve.setText("" + editedRectiept.getRecieved());
        edtTotal.setText("" + editedRectiept.getTotal());
    }
    
    private void setFormToObj() {
//        Date date = new Date();
//     
        editedRectiept.setDateS(edtDate.getText());
        editedRectiept.setTimeS(edtTime.getText());
        editedRectiept.setDiscount(Double.parseDouble(edtDiscount.getText()));
        editedRectiept.setChange(Double.parseDouble(edtDiscount.getText()));
        editedRectiept.setPaymant(cmbPayment.getSelectedItem() + "");
        editedRectiept.setEm_Id(reciptService.getRecEmp(edtempid.getText()));
        editedRectiept.setCu_Id(reciptService.getRecCus(edtcusid.getText()));
        editedRectiept.setSt_Id(reciptService.getRecStore(edtstid.getText()));
        editedRectiept.setQueue(Integer.parseInt(edtqueue.getText()));
        editedRectiept.setRecieved(Double.parseDouble(edtRecieve.getText()));
    }
    
    public void refreshTable() {
        reciepList = reciptService.getReciept();
        tblReciept.revalidate();
        tblReciept.repaint();
    }
    
    private void setObjToFormEdit() {
        txtId.setText("" + editedRectiept.getId());
        edtDate.setText(editedRectiept.getDateS());
        edtTime.setText(editedRectiept.getTimeS());
        edtempid.setText("" + editedRectiept.getEm_Id().getId());
        edtstid.setText("" + editedRectiept.getSt_Id().getId());
        edtcusid.setText("" + editedRectiept.getCu_Id().getId());
        edtqueue.setText("" + editedRectiept.getQueue());
        edtDiscount.setText("" + editedRectiept.getDiscount());
        edtTotal.setText("" + editedRectiept.getTotal());
        edtRecieve.setText("" + editedRectiept.getRecieved());
        edtChange.setText("" + editedRectiept.getChange());
        if (editedRectiept.getPaymant().equals("P")) {
            cmbPayment.setSelectedIndex(0);
        } else {
            cmbPayment.setSelectedIndex(1);
        }
    }
}
