/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mukku.dcoffee7.ui;

import com.mukku.dcoffee7.model.CheckInOut;
import com.mukku.dcoffee7.model.EmployeeOverdue;
import com.mukku.dcoffee7.model.SummarySalary;
import com.mukku.dcoffee7.service.CheckInOutService;
import com.mukku.dcoffee7.service.EmployeeOverdueService;
import com.mukku.dcoffee7.service.SummarySalaryService;
import static java.lang.Double.parseDouble;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Acer
 */
public class SummarySalaryPanel extends javax.swing.JPanel {
    
    private final SummarySalaryService ssService;
    private final EmployeeOverdueService eoService;
    private final CheckInOutService cioService;
    private List<SummarySalary> listSs;
    private List<EmployeeOverdue> listEmployeeOverdue;
    private SummarySalary editedSummarySalary;
    private EmployeeOverdue editedEmployeeOverdue;
    private SummarySalary summarySalary;
    private CheckInOut editedCio;

    /**
     * Creates new form SummarySalaryPanel
     */
    public SummarySalaryPanel() {
        initComponents();
        ssService = new SummarySalaryService();
        eoService = new EmployeeOverdueService();
        cioService = new CheckInOutService();
        listSs = ssService.getSummarySalary();
        tblSummarySalary.setModel(new AbstractTableModel() {
            String[] columnName = {"ID", "Date", "Work Hour", "Salary"};
            
            @Override
            public String getColumnName(int column) {
                return columnName[column];
            }
            
            @Override
            public int getRowCount() {
                return listSs.size();
            }
            
            @Override
            public int getColumnCount() {
                return 4;
            }
            
            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                SummarySalary summarySalary = listSs.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return summarySalary.getId();
                    case 1:
                        return summarySalary.getDate();
                    case 2:
                        return summarySalary.getWorkHour();
                    case 3:
                        return summarySalary.getSalary();
                    default:
                        return "Unknown";
                }
            }
            
        });
        
        listEmployeeOverdue = eoService.getEmployeeOverdue();
        tblEmployeeOverdue.setModel(new AbstractTableModel() {
            String[] columnName = {"Employee ID", "Employee Name", "Total Work Time", "Hourly Wage", "Salary"};
            
            @Override
            public String getColumnName(int column) {
                return columnName[column];
            }
            
            @Override
            public int getRowCount() {
                return listEmployeeOverdue.size();
            }
            
            @Override
            public int getColumnCount() {
                return 5;
            }
            
            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                EmployeeOverdue employeeOverdue = listEmployeeOverdue.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return employeeOverdue.getEmpId();
                    case 1:
                        return employeeOverdue.getEmpName();
                    case 2:
                        return employeeOverdue.getTotalHour();
                    case 3:
                        return employeeOverdue.getHourlyWage();                    
                    case 4:
                        return employeeOverdue.getSalary();
                    default:
                        return "Unknown";
                }
            }
            
        });
        
        enableForm(false);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        txtWorkHour = new javax.swing.JTextField();
        btnSave = new javax.swing.JButton();
        btnClear = new javax.swing.JButton();
        txtSalary = new javax.swing.JTextField();
        txtId = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblSummarySalary = new javax.swing.JTable();
        jPanel2 = new javax.swing.JPanel();
        btnDelete = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblEmployeeOverdue = new javax.swing.JTable();
        jPanel4 = new javax.swing.JPanel();
        btnAdd = new javax.swing.JButton();

        setPreferredSize(new java.awt.Dimension(575, 575));

        jPanel1.setBackground(new java.awt.Color(228, 209, 161));

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel5.setText("ID:");

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel6.setText("Work Hour:");

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel7.setText("Salary:");

        txtWorkHour.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        btnSave.setBackground(new java.awt.Color(255, 255, 255));
        btnSave.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btnSave.setText("Save");
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        btnClear.setBackground(new java.awt.Color(255, 255, 255));
        btnClear.setText("Clear");
        btnClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearActionPerformed(evt);
            }
        });

        txtSalary.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        txtId.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(btnSave)
                .addGap(18, 18, 18)
                .addComponent(btnClear)
                .addGap(21, 21, 21))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel7)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtId)
                        .addGap(80, 80, 80)
                        .addComponent(jLabel6)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtSalary, javax.swing.GroupLayout.DEFAULT_SIZE, 216, Short.MAX_VALUE)
                    .addComponent(txtWorkHour))
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(jLabel6)
                    .addComponent(txtWorkHour, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtId))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(txtSalary, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(4, 4, 4)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSave)
                    .addComponent(btnClear))
                .addContainerGap())
        );

        tblSummarySalary.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        tblSummarySalary.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tblSummarySalary);

        jPanel2.setBackground(new java.awt.Color(228, 209, 161));

        btnDelete.setBackground(new java.awt.Color(255, 255, 255));
        btnDelete.setText("Delete");
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnDelete)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnDelete)
                .addContainerGap(15, Short.MAX_VALUE))
        );

        jPanel3.setBackground(new java.awt.Color(203, 164, 115));

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel4.setText("Employee Overdue");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel4)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel4)
                .addContainerGap())
        );

        tblEmployeeOverdue.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        tblEmployeeOverdue.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(tblEmployeeOverdue);

        jPanel4.setBackground(new java.awt.Color(203, 164, 115));

        btnAdd.setBackground(new java.awt.Color(255, 255, 255));
        btnAdd.setText("Add");
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnAdd)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnAdd)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 599, Short.MAX_VALUE)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING)
            .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 184, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
        int selectedIndex = tblEmployeeOverdue.getSelectedRow();
        if (selectedIndex >= 0) {
            editedEmployeeOverdue = listEmployeeOverdue.get(selectedIndex);
            setObjectToForm();
            enableForm(true);
        }
    }//GEN-LAST:event_btnAddActionPerformed

    private void btnClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearActionPerformed
        enableForm(false);
        editedEmployeeOverdue = null;
    }//GEN-LAST:event_btnClearActionPerformed

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        setFormToObject();
        enableForm(false);
        summarySalary = ssService.addNew(editedSummarySalary);
        updateCioSsId();
        refreshTable();
    }//GEN-LAST:event_btnSaveActionPerformed

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        int selectedIndex = tblSummarySalary.getSelectedRow();
        if (selectedIndex >= 0) {
            editedSummarySalary = listSs.get(selectedIndex);
            int input = JOptionPane.showConfirmDialog(this, "Do you want to delete?", "Select an Option...",
                    JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE);
            if (input == 0) {
                editedCio = new CheckInOut();
                editedCio.setSsId(editedSummarySalary.getId());
                cioService.updateNullSsId(editedCio);
                ssService.delete(editedSummarySalary);
            }
        }
        refreshTable();
    }//GEN-LAST:event_btnDeleteActionPerformed
    
    private void updateCioSsId() {
        editedCio = new CheckInOut();
        editedCio.setSsId(summarySalary.getId());
        cioService.updateSsId(editedCio, editedEmployeeOverdue.getEmpId());
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnClear;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnSave;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable tblEmployeeOverdue;
    private javax.swing.JTable tblSummarySalary;
    private javax.swing.JLabel txtId;
    private javax.swing.JTextField txtSalary;
    private javax.swing.JTextField txtWorkHour;
    // End of variables declaration//GEN-END:variables

    private void enableForm(boolean status) {
        if (!status) {
            txtWorkHour.setText("");
            txtSalary.setText("");
            txtWorkHour.setEnabled(status);
            txtSalary.setEnabled(status);
        }
        btnSave.setEnabled(status);
        btnClear.setEnabled(status);
    }
    
    private void setObjectToForm() {
        editedSummarySalary = new SummarySalary();
        txtId.setText(editedSummarySalary.getId() + "");
        txtWorkHour.setText(editedEmployeeOverdue.getTotalHour());
        txtSalary.setText(editedEmployeeOverdue.getSalary() + "");
    }
    
    private void setFormToObject() {
        editedSummarySalary = new SummarySalary();
        editedSummarySalary.setWorkHour(txtWorkHour.getText());
        editedSummarySalary.setSalary(parseDouble(txtSalary.getText()));
    }
    
    private void refreshTable() {
        listSs = ssService.getSummarySalary();
        tblSummarySalary.revalidate();
        tblSummarySalary.repaint();
        
        listEmployeeOverdue = eoService.getEmployeeOverdue();
        tblEmployeeOverdue.revalidate();
        tblEmployeeOverdue.repaint();
    }
}
