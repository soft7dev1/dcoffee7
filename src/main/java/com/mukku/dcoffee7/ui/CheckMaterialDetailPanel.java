/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package com.mukku.dcoffee7.ui;

import com.mukku.dcoffee7.model.ChMaterialDetail;
import com.mukku.dcoffee7.service.ChMatDetailService;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author acer
 */
public class CheckMaterialDetailPanel extends javax.swing.JPanel {

    private MainFrame mainFrame;
    private final ChMatDetailService chMatDetailService;
    private List<ChMaterialDetail> chMatDtList;
    private int selectId;
    private ChMaterialDetail editedChMatDt;
    private int enableTextAuto = 0;

    /**
     * Creates new form UserPanel
     *
     * @param mainFrame
     * @param chMatId
     */
    public CheckMaterialDetailPanel(MainFrame mainFrame, int chMatId) {
        initComponents();
        this.mainFrame = mainFrame;
        this.selectId = chMatId;
//        System.out.println("check" + selectId);
        chMatDetailService = new ChMatDetailService();
        chMatDtList = chMatDetailService.getChMaterialDetails(selectId);

        tblChMatDetail.setModel(new AbstractTableModel() {
            @Override
            public String getColumnName(int column) {
                String[] columnName = {"ID", "Material Name", "Last Quantity", "Remain Quantity", "Material ID"};//, "Check Material ID"
                return columnName[column];
            }

            @Override
            public int getRowCount() {
                return chMatDtList.size();
            }

            @Override
            public int getColumnCount() {
                return 5;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                ChMaterialDetail chMatDt = chMatDtList.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return chMatDt.getId();
                    case 1:
                        return chMatDt.getMatName();
                    case 2:
                        return chMatDt.getQtyLast();
                    case 3:
                        return chMatDt.getQtyRemain();
                    case 4:
                        return chMatDt.getMaterial().getId();
//                    case 5:
//                        return chMatDt.getChMaterial().getId();
                    default:
                        return "Unknown";
                }
            }
        });

        enableForm(false);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnGender = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtId = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        edtLastQty = new javax.swing.JTextField();
        edtName = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        btnSave = new javax.swing.JButton();
        btnClear = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        edtMatId = new javax.swing.JTextField();
        edtRemQty = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        edtChMatId = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblChMatDetail = new javax.swing.JTable();
        jPanel2 = new javax.swing.JPanel();
        btnAdd = new javax.swing.JButton();
        btnEdit = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        btnBack = new javax.swing.JButton();

        jPanel1.setBackground(new java.awt.Color(228, 209, 161));

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel1.setText("ID : ");

        txtId.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txtId.setText("      ");

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel3.setText("Last Quantity :");

        edtLastQty.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        edtLastQty.setText("<auto>");

        edtName.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        edtName.setText("<auto>");

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel4.setText("Material Name : ");

        btnSave.setBackground(new java.awt.Color(255, 255, 255));
        btnSave.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btnSave.setText("Save");
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        btnClear.setBackground(new java.awt.Color(255, 255, 255));
        btnClear.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btnClear.setText("Clear");
        btnClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearActionPerformed(evt);
            }
        });

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel7.setText("Remain Quantity :");

        jLabel8.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel8.setText("Material ID :");

        edtMatId.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        edtRemQty.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        jLabel9.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel9.setText("Check Material ID :");

        edtChMatId.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        edtChMatId.setText("<auto>");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtId, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.LEADING))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(edtName, javax.swing.GroupLayout.DEFAULT_SIZE, 92, Short.MAX_VALUE)
                    .addComponent(edtLastQty)
                    .addComponent(edtRemQty))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnSave)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnClear))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel9)
                            .addComponent(jLabel8))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(edtMatId, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(edtChMatId, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 145, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel1)
                        .addComponent(txtId))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel4)
                        .addComponent(edtName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel9)
                        .addComponent(edtChMatId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(edtLastQty, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3)
                    .addComponent(jLabel8)
                    .addComponent(edtMatId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(edtRemQty, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7))
                .addContainerGap(25, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSave)
                    .addComponent(btnClear))
                .addContainerGap())
        );

        tblChMatDetail.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        tblChMatDetail.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {
                "ID", "Material Name", "Last Quantity", "Remain Quantity", "Material ID", "Check Material ID"
            }
        ));
        jScrollPane1.setViewportView(tblChMatDetail);

        jPanel2.setBackground(new java.awt.Color(228, 209, 161));

        btnAdd.setBackground(new java.awt.Color(255, 255, 255));
        btnAdd.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btnAdd.setText("Add");
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });

        btnEdit.setBackground(new java.awt.Color(255, 255, 255));
        btnEdit.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btnEdit.setText("Edit");
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });

        btnDelete.setBackground(new java.awt.Color(255, 255, 255));
        btnDelete.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btnDelete.setText("Delete");
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });

        btnBack.setBackground(new java.awt.Color(255, 255, 255));
        btnBack.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btnBack.setText("Back");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnAdd)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnEdit)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnDelete)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnBack)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAdd)
                    .addComponent(btnEdit)
                    .addComponent(btnDelete)
                    .addComponent(btnBack))
                .addContainerGap(16, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 243, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
        editedChMatDt = new ChMaterialDetail();
        setObjToFormAdd();
        enableTextAuto = 1;
        enableForm(true);
    }//GEN-LAST:event_btnAddActionPerformed

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        if (edtMatId.getText().equals("") || edtRemQty.getText().equals("")) {
            JOptionPane.showMessageDialog(this, "Please input correct data");
            edtMatId.requestFocus();
            return;
        }
        if (editedChMatDt.getId() < 0) {
            setFormToObjAdd();
            chMatDetailService.addChMaterialDetail(editedChMatDt);
        } else {
            setFormToObjEdit();
            chMatDetailService.updateChMaterialDetail(editedChMatDt);
        }
        enableForm(false);
        refreshTable();
    }//GEN-LAST:event_btnSaveActionPerformed

    private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
        int selectIndex = tblChMatDetail.getSelectedRow();
        if (selectIndex >= 0) {
            editedChMatDt = chMatDtList.get(selectIndex);
            setObjToFormEdit();
            enableTextAuto = 0;
            enableForm(true);
        }
    }//GEN-LAST:event_btnEditActionPerformed

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        int selectIndex = tblChMatDetail.getSelectedRow();
        if (selectIndex >= 0) {
            editedChMatDt = chMatDtList.get(selectIndex);
            int input = JOptionPane.showConfirmDialog(this, "Do you want to proceed?", "Select an Option...", JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE);
//            System.out.println(input);
            if (input == 0) {
                chMatDetailService.deleteChMaterialDetail(editedChMatDt);
            }
            refreshTable();
        }
    }//GEN-LAST:event_btnDeleteActionPerformed

    private void btnClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearActionPerformed
        enableForm(false);
        editedChMatDt = null;
    }//GEN-LAST:event_btnClearActionPerformed

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        mainFrame.setVisible(false);
    }//GEN-LAST:event_btnBackActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnClear;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnEdit;
    private javax.swing.ButtonGroup btnGender;
    private javax.swing.JButton btnSave;
    private javax.swing.JTextField edtChMatId;
    private javax.swing.JTextField edtLastQty;
    private javax.swing.JTextField edtMatId;
    private javax.swing.JTextField edtName;
    private javax.swing.JTextField edtRemQty;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tblChMatDetail;
    private javax.swing.JLabel txtId;
    // End of variables declaration//GEN-END:variables

    private void setObjToFormAdd() {
        txtId.setText("" + editedChMatDt.getId());
//        edtMatId.setText("" + editedChMatDt.getMaterial().getId());
//        edtName.setText(editedChMatDt.getMaterial().getName());
//        edtLastQty.setText("" + editedChMatDt.getQtyLast());
        edtRemQty.setText("" + editedChMatDt.getQtyRemain());
        edtChMatId.setText("" + selectId);
    }

    private void setFormToObjAdd() {
        editedChMatDt.setMaterial(chMatDetailService.getChMatDtMatr(edtMatId.getText()));
//        editedChMatDt.setMatName(edtName.getText());
//        editedChMatDt.setQtyLast(Integer.parseInt(edtLastQty.getText()));
        editedChMatDt.setQtyRemain(Integer.parseInt(edtRemQty.getText()));
        editedChMatDt.setChMaterial(chMatDetailService.getChMatDtChMat(edtChMatId.getText()));
    }

    private void refreshTable() {
        chMatDtList = chMatDetailService.getChMaterialDetails(selectId);
        tblChMatDetail.revalidate();
        tblChMatDetail.repaint();
    }

    private void setObjToFormEdit() {
        txtId.setText("" + editedChMatDt.getId());
        edtMatId.setText("" + editedChMatDt.getMaterial().getId());
        edtName.setText(editedChMatDt.getMatName());
        edtLastQty.setText("" + editedChMatDt.getQtyLast());
        edtRemQty.setText("" + editedChMatDt.getQtyRemain());
        edtChMatId.setText("" + selectId);
    }

    private void setFormToObjEdit() {
        editedChMatDt.setMaterial(chMatDetailService.getChMatDtMatr(edtMatId.getText()));
        editedChMatDt.setMatName(edtName.getText());
//        editedChMatDt.setQtyLast(Integer.parseInt(edtLastQty.getText()));
        editedChMatDt.setQtyLast(chMatDetailService.getChMatDtMatr(edtMatId.getText()).getQuantity());
        editedChMatDt.setQtyRemain(Integer.parseInt(edtRemQty.getText()));
        editedChMatDt.setChMaterial(chMatDetailService.getChMatDtChMat(edtChMatId.getText()));
    }

    private void enableForm(boolean stt) {
        if (stt == false) {
            edtMatId.setText("");
            edtMatId.setEnabled(stt);
            edtName.setText("<auto>");
            edtLastQty.setText("<auto>");
            edtRemQty.setText("");
//            edtChMatId.setText("");
        }
        if (stt == true) {
            if (enableTextAuto == 1) {
                edtName.setText("<auto>");
                edtMatId.setEnabled(stt);
            } else {
                edtLastQty.setText("<auto>");
                edtMatId.setEnabled(false);
            }
        }
//        edtMatId.setEnabled(stt);
        edtName.setEnabled(stt);
        edtLastQty.setEnabled(stt);
        edtRemQty.setEnabled(stt);
        edtChMatId.setEnabled(false);
        btnSave.setEnabled(stt);
        btnClear.setEnabled(stt);
        edtRemQty.requestFocus();
    }
}
