/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mukku.dcoffee7.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ทักช์ติโชค
 */
public class MTypeCusReport {

    private String month;
    private int isMember;
    private int isNotMember;

    public MTypeCusReport(String month, int isMember, int isNotMember) {
        this.month = month;
        this.isMember = isMember;
        this.isNotMember = isNotMember;
    }

    public MTypeCusReport() {
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public int getIsMember() {
        return isMember;
    }

    public void setIsMember(int isMember) {
        this.isMember = isMember;
    }

    public int getIsNotMember() {
        return isNotMember;
    }

    public void setIsNotMember(int isNotMember) {
        this.isNotMember = isNotMember;
    }

    @Override
    public String toString() {
        return "MTypeCusReport{" + "month=" + month + ", isMember=" + isMember + ", isNotMember=" + isNotMember + '}';
    }

    public static MTypeCusReport fromRS(ResultSet rs) {
        MTypeCusReport mtcReport = new MTypeCusReport();
        try {
            mtcReport.setMonth(rs.getString("month"));
            mtcReport.setIsMember(rs.getInt("is_member"));
            mtcReport.setIsNotMember(rs.getInt("is_not_member"));
        } catch (SQLException ex) {
            Logger.getLogger(MTypeCusReport.class.getName()).log(Level.SEVERE, null, ex);
        }
        return mtcReport;
    }

}
