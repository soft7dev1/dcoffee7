/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mukku.dcoffee7.model;

import com.mukku.dcoffee7.dao.CustomerDao;
import com.mukku.dcoffee7.dao.EmployeeDao;
import com.mukku.dcoffee7.dao.StoreDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.sqlite.date.FastDateFormat;

/**
 *
 * @author acer
 */
public class Reciept {

    private int Id;
    private int queue;
    private Date date;
    private Date time;
    private ArrayList<RecieptDetail> recieptDetail;
    private double discount;
    private double total;
    private double recieved;
    private double change;
    private String paymant;
    private String dateS;
    private String timeS;
    private Store st_Id;
    private Employee em_Id;
    private Customer cu_Id;
    private static final DecimalFormat df = new DecimalFormat("0.00");

    public Reciept(int Id, int queue, Date date, Date time, ArrayList<RecieptDetail> recieptDetail, double discount, double total, double recieved, double change, String paymant, Store st_Id, Employee em_Id, Customer cu_Id) {
        this.Id = Id;
        this.queue = queue;
        this.date = date;
        this.time = time;
        this.recieptDetail = recieptDetail;
        this.discount = discount;
        this.total = total;
        this.recieved = recieved;
        this.change = change;
        this.paymant = paymant;
        this.st_Id = st_Id;
        this.em_Id = em_Id;
        this.cu_Id = cu_Id;
    }

    public Reciept(int queue, double discount, double total, double recieved, double change, String paymant, Store st_Id, Employee em_Id, Customer cu_Id) {
        this.Id = -1;
        this.queue = queue;
        recieptDetail = new ArrayList<>();
        this.discount = discount;
        this.total = total;
        this.recieved = recieved;
        this.change = change;
        this.paymant = paymant;
        this.st_Id = st_Id;
        this.em_Id = em_Id;
        this.cu_Id = cu_Id;
        date = new Date();
        dateS = FastDateFormat.getInstance("dd/MM/yyyy").format(date);
        time = new Date();
        timeS = FastDateFormat.getInstance("HH:mm").format(time);

    }

    public Reciept(double discount, double total, double recieved, double change, String paymant, Store st_Id, Employee em_Id, Customer cu_Id) {
        this.Id = -1;
        this.queue = -1;
        recieptDetail = new ArrayList<>();
        this.discount = discount;
        this.total = total;
        this.recieved = recieved;
        this.change = change;
        this.paymant = paymant;
        this.st_Id = st_Id;
        this.em_Id = em_Id;
        this.cu_Id = cu_Id;
        date = new Date();
        dateS = FastDateFormat.getInstance("dd/MM/yyyy").format(date);
        time = new Date();
        timeS = FastDateFormat.getInstance("HH:mm").format(time);

    }

    public Reciept(int Id, int queue, Date date, Date time, double discount, double total, double recieved, double change, String paymant, Store st_Id, Employee em_Id, Customer cu_Id) {
        this.Id = Id;
        this.queue = queue;
        this.date = date;
        this.time = time;
        this.discount = discount;
        this.total = total;
        this.recieved = recieved;
        this.change = change;
        this.paymant = paymant;
        this.st_Id = st_Id;
        this.em_Id = em_Id;
        this.cu_Id = cu_Id;
    }

    public Reciept(int queue, Date date, Date time, double discount, double total, double recieved, double change, String paymant, Store st_Id, Employee em_Id, Customer cu_Id) {
        this.Id = -1;
        this.queue = queue;
        this.date = date;
        this.time = time;
        this.discount = discount;
        this.total = total;
        this.recieved = recieved;
        this.change = change;
        this.paymant = paymant;
        this.st_Id = st_Id;
        this.em_Id = em_Id;
        this.cu_Id = cu_Id;
    }

    public Reciept(Store st_Id, Employee em_Id, Customer cu_Id) {
        this.Id = -1;
        this.st_Id = st_Id;
        this.em_Id = em_Id;
        date = new Date();
        dateS = FastDateFormat.getInstance("dd/MM/yyyy").format(date);
        time = new Date();
        timeS = FastDateFormat.getInstance("HH:mm").format(time);
        this.queue = -1;
        this.discount = 0;
        this.total = 0;
        this.recieved = 0;
        this.change = 0;
        this.paymant = "";
        this.cu_Id = cu_Id;
        recieptDetail = new ArrayList<>();
    }

    public Reciept() {
        this.Id = -1;
        recieptDetail = new ArrayList<>();
    }

    public String getDateS() {
        return dateS;
    }

    public void setDateS(String dateS) {
        this.dateS = dateS;
    }

    public String getTimeS() {
        return timeS;
    }

    public void setTimeS(String timeS) {
        this.timeS = timeS;
    }

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public int getQueue() {
        return queue;
    }

    public void setQueue(int queue) {
        this.queue = queue;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public double getRecieved() {
        return recieved;
    }

    public void setRecieved(double recieved) {
        this.recieved = recieved;
    }

    public double getChange() {
        if (recieved >= total) {
            return recieved - (total);
        }
        return Double.parseDouble(df.format(change));
    }

    public void setChange(double change) {
        this.change = change;
    }

    public String getPaymant() {
        return paymant;
    }

    public void setPaymant(String paymant) {
        this.paymant = paymant;
    }

    public Store getSt_Id() {
        return st_Id;
    }

    public void setSt_Id(Store st_Id) {
        this.st_Id = st_Id;
    }

    public Employee getEm_Id() {
        return em_Id;
    }

    public void setEm_Id(Employee em_Id) {
        this.em_Id = em_Id;
    }

    public Customer getCu_Id() {
        return cu_Id;
    }

    public void setCu_Id(Customer cu_Id) {
        this.cu_Id = cu_Id;
    }

    public ArrayList<RecieptDetail> getRecieptDetail() {
        return recieptDetail;
    }

    public void setRecieptDetail(ArrayList<RecieptDetail> recieptDetail) {
        this.recieptDetail = recieptDetail;
    }

    @Override
    public String toString() {
        return "Reciept{" + "Id=" + Id + ", queue=" + queue + ", date=" + dateS + ", time=" + timeS + ", discount=" + discount + ", total=" + total + ", recieved=" + recieved + ", change=" + change + ", paymant=" + paymant + ", st_Id=" + st_Id + ", em_Id=" + em_Id + ", cu_Id=" + cu_Id + '}';
    }

    public void addRecieptDetail(RecieptDetail recieptdetail) {
        recieptDetail.add(recieptdetail);
        total = total + recieptdetail.getTotals();

    }

    public void addRecieptDetail(Product product, String productName, double productPrice, int amount) {
        RecieptDetail recieptDetail = new RecieptDetail(product, productName, amount, productPrice, this);
        this.addRecieptDetail(recieptDetail);
    }

    public void addRecieptDetail(Product product, int amount) {
        RecieptDetail recieptDetail = new RecieptDetail(product, product.getName(), amount, product.getPrice(), this);
        this.addRecieptDetail(recieptDetail);
    }

    public static Reciept fromRS(ResultSet rs) {
        EmployeeDao employeeDao = new EmployeeDao();
        StoreDao storeDao = new StoreDao();
        CustomerDao cutomerDao = new CustomerDao();
        Reciept reciept = new Reciept();
        try {
            reciept.setId(rs.getInt("rec_id"));
            reciept.setQueue(rs.getInt("rec_queue"));
            //  reciept.setDate(rs.getDate("rec_date"));
            reciept.setDateS(rs.getString("rec_date"));
            //   reciept.setTime(rs.getDate("rec_time"));
            reciept.setTimeS(rs.getString("rec_time"));
            reciept.setDiscount(rs.getDouble("rec_discount"));
            reciept.setTotal(rs.getDouble("rec_total"));
            reciept.setRecieved(rs.getDouble("rec_recieved"));
            reciept.setChange(rs.getDouble("rec_change"));
            reciept.setPaymant(rs.getString("rec_payment"));
            int store = rs.getInt("store_id");
            Store item = storeDao.get(store);
            reciept.setSt_Id(item);
            int employee = rs.getInt("employee_id");
            Employee item2 = employeeDao.get(employee);
            reciept.setEm_Id(item2);
            int customer = rs.getInt("customer_id");
            Customer item3 = cutomerDao.get(customer);
            reciept.setCu_Id(item3);

        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return reciept;
    }
}
