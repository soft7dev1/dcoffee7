/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mukku.dcoffee7.model;

import com.mukku.dcoffee7.dao.CheckMatDao;
import com.mukku.dcoffee7.dao.MaterialDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author acer
 */
public class ChMaterialDetail {

    private int id;
    private Material material;
    private String matName;
    private int qtyLast;
    private int qtyRemain;
    private CheckMaterial chMaterial;

    public ChMaterialDetail(int id, Material material, String matName, int qtyLast, int qtyRemain, CheckMaterial chMaterial) {
        this.id = id;
        this.material = material;
        this.matName = matName;
        this.qtyLast = qtyLast;
        this.qtyRemain = qtyRemain;
        this.chMaterial = chMaterial;
    }

    public ChMaterialDetail() {
        this.id = -1;
    }

    public ChMaterialDetail(Material material, String matName, int qtyLast, int qtyRemain, CheckMaterial chMaterial) {
        this.id = -1;
        this.material = material;
        this.matName = matName;
        this.qtyLast = qtyLast;
        this.qtyRemain = qtyRemain;
        this.chMaterial = chMaterial;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Material getMaterial() {
        return material;
    }

    public void setMaterial(Material material) {
        this.material = material;
    }

    public String getMatName() {
        return matName;
    }

    public void setMatName(String matName) {
        this.matName = matName;
    }

    public int getQtyLast() {
        return qtyLast;
    }

    public void setQtyLast(int qtyLast) {
        this.qtyLast = qtyLast;
    }

    public int getQtyRemain() {
        return qtyRemain;
    }

    public void setQtyRemain(int qtyRemain) {
        this.qtyRemain = qtyRemain;
    }

    public CheckMaterial getChMaterial() {
        return chMaterial;
    }

    public void setChMaterial(CheckMaterial chMaterial) {
        this.chMaterial = chMaterial;
    }

    @Override
    public String toString() {
        return "ChMaterialDetail{" + "id=" + id + ", matName=" + matName + ", qtyLast=" + qtyLast + ", qtyRemain=" + qtyRemain + ", material=" + material + '}';
    }

    public static ChMaterialDetail fromRS(ResultSet rs) {
        MaterialDao materialDao = new MaterialDao();
        ChMaterialDetail chMatDetail = new ChMaterialDetail();
//        CheckMatDao checkMatDao = new CheckMatDao();
        try {
            chMatDetail.setId(rs.getInt("cmd_id"));
            int materialId = rs.getInt("mat_id");
            Material mat = materialDao.get(materialId);
            chMatDetail.setMaterial(mat);
            chMatDetail.setMatName(rs.getString("cmd_name"));
            chMatDetail.setQtyLast(rs.getInt("cmd_qty_last"));
            chMatDetail.setQtyRemain(rs.getInt("cmd_qty_remain"));
//            int chMatId = rs.getInt("check_mat_id");
//            CheckMaterial chMat = checkMatDao.get(chMatId);
//            chMatDetailDao.setChMaterial(chMat);
        } catch (SQLException ex) {
            Logger.getLogger(ChMaterialDetail.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return chMatDetail;
    }

}
