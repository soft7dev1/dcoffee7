/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mukku.dcoffee7.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jakka
 */
public class Material {

    private int id;
    private String name;
    private int min_quantity;
    private int quantity;
    private String unit;
    private double price_per_unit;

    public Material(int id, String name, int min_quantity, int quantity, String unit, double price_per_unit) {
        this.id = id;
        this.name = name;
        this.min_quantity = min_quantity;
        this.quantity = quantity;
        this.unit = unit;
        this.price_per_unit = price_per_unit;

    }

    public Material(String name, int min_quantity, int quantity, String unit, double price_per_unit) {
        this.name = name;
        this.min_quantity = min_quantity;
        this.quantity = quantity;
        this.unit = unit;
        this.price_per_unit = price_per_unit;

    }

    public Material() {
        this.id = -1;
        this.name = "";
        this.min_quantity = 0;
        this.quantity = 0;
        this.unit = "";
        this.price_per_unit = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMin_quantity() {
        return min_quantity;
    }

    public void setMin_quantity(int min_quantity) {
        this.min_quantity = min_quantity;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Double getPrice_per_unit() {
        return price_per_unit;
    }

    public void setPrice_per_unit(Double price_per_unit) {
        this.price_per_unit = price_per_unit;
    }

    @Override
    public String toString() {
        return "Material{" + "id=" + id + ", name=" + name + ", min_quantity=" + min_quantity + ", quantity=" + quantity + ", unit=" + unit + ", price_per_unit=" + price_per_unit + '}';
    }

    public static Material fromRS(ResultSet rs) {
        Material Material = new Material();
        try {
            Material.setId(rs.getInt("mat_id"));
            Material.setMin_quantity(rs.getInt("mat_min_quantity"));
            Material.setName(rs.getString("mat_name"));
            Material.setQuantity(rs.getInt("mat_quantity"));
            Material.setUnit(rs.getString("mat_unit"));
            Material.setPrice_per_unit(rs.getDouble("mat_price_per_unit"));
        } catch (SQLException ex) {
            Logger.getLogger(Material.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return Material;
    }
}
