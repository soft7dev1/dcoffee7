/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mukku.dcoffee7.model;

import com.mukku.dcoffee7.dao.ProductDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author acer
 */
public class RecieptDetail {

    private int id;
    Product product;
    private String name;
    private int amount;
    private double price;
    private double total;
    //private int product_id;
    //   private int rec_id;
    private Reciept reciept;

//    public recieptDetail(Product product, int amount, double total, ereciept1 reciept) {
//        this.product = product;
//        this.amount = amount;
//        this.price = price;
//        this.total = total;
//        this.reciept = reciept;
//    }
    public RecieptDetail(int id, Product product, String name, int amount, double price, double total, Reciept reciept) {
        this.id = id;
        this.product = product;
        this.name = name;
        this.amount = amount;
        this.price = price;
        this.total = total;

        this.reciept = reciept;
    }

    public RecieptDetail(Product product, String name, int amount, double price, double total, Reciept reciept) {
        this.product = product;
        this.name = name;
        this.amount = amount;
        this.price = price;
        this.total = total;

        this.reciept = reciept;
    }

    public RecieptDetail() {
        this.id = -1;
    }

    public RecieptDetail(Product product, String name, int amount, double price, Reciept reciept) {
        this.product = product;
        this.name = name;
        this.amount = amount;
        this.price = price;
        this.reciept = reciept;
    }

    public RecieptDetail(Product product, String name, int amount, double price) {
        this.product = product;
        this.name = name;
        this.amount = amount;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getTotal() {
        return price * amount;
    }

    public void setTotal(double total) {
        this.total = total;

    }

    public Reciept getReciept() {
        return reciept;
    }

    public void setReciept(Reciept reciept) {
        this.reciept = reciept;
    }

    public double getTotals() {
        return amount * price;
    }

    // amount*price;
    @Override
    public String toString() {
        return "RecieptDetail{" + "id=" + id + ", product=" + product + ", name=" + name + ", amount=" + amount + ", price=" + price + ", total=" + total + '}';
    }

    public static RecieptDetail fromRS(ResultSet rs) {
        ProductDao productDao = new ProductDao();
        RecieptDetail recieptdetail = new RecieptDetail();
        try {
            recieptdetail.setId(rs.getInt("rcd_id"));
            recieptdetail.setName(rs.getString("rcd_name"));
            recieptdetail.setAmount(rs.getInt("rcd_amount"));
            recieptdetail.setPrice(rs.getDouble("rcd_price"));
            recieptdetail.setTotal(rs.getDouble("rcd_total"));
            int productId = rs.getInt("product_id");
            Product item = productDao.get(productId);
            recieptdetail.setProduct(item);
            // int recietdetail=rs.getInt("rec_id");

        } catch (SQLException ex) {
            Logger.getLogger(RecieptDetail.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return recieptdetail;
    }
}
