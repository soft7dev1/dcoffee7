/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mukku.dcoffee7.model;

import com.mukku.dcoffee7.dao.EmployeeDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.sqlite.date.FastDateFormat;

/**
 *
 * @author acer
 */
public class Bill {

    private int id;
    private String shopName;
    private Date date;
    private Date time;
    private String dateS;
    private String timeS;
    private double total;
    private double buy;
    private double change;
    ArrayList<BillDetail> billMatDetails;
    private Employee employee;

    public Bill(int id, String shopName, Date date, Date time, double total, double buy, double change, ArrayList<BillDetail> billMatDetails, Employee employee) {
        this.id = id;
        this.shopName = shopName;
        this.date = date;
        this.time = time;
        this.total = total;
        this.buy = buy;
        this.change = change;
        this.billMatDetails = billMatDetails;
        this.employee = employee;
    }

    public Bill() {
        this.id = -1;
    }

    public Bill(String shopName, Date date, Date time, double total, double buy, double change, ArrayList<BillDetail> billMatDetails, Employee employee) {
        this.shopName = shopName;
        this.date = date;
        this.time = time;
        this.total = total;
        this.buy = buy;
        this.change = change;
        this.billMatDetails = billMatDetails;
        this.employee = employee;
    }

    public Bill(String shopName, Employee employee) {
        this.id = -1;
        billMatDetails = new ArrayList<>();
        date = new Date();
        dateS = FastDateFormat.getInstance("dd/MM/yyyy").format(date);
        time = new Date();
        timeS = FastDateFormat.getInstance("HH:mm").format(time);
        this.total = 0;
        this.buy = 0;
        this.change = 0;
        this.shopName = shopName;
        this.employee = employee;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getDateS() {
        return dateS;
    }

    public void setDateS(String dateS) {
        this.dateS = dateS;
    }

    public String getTimeS() {
        return timeS;
    }

    public void setTimeS(String timeS) {
        this.timeS = timeS;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public double getBuy() {
        return buy;
    }

    public void setBuy(double buy) {
        this.buy = buy;
    }

    public double getChange() {
        if (buy >= total) {
            return buy - total;
        }
        return change;
    }

    public void setChange(double change) {
        this.change = change;
    }

    public ArrayList<BillDetail> getBillMatDetails() {
        return billMatDetails;
    }

    public void setBillMatDetails(ArrayList<BillDetail> billMatDetails) {
        this.billMatDetails = billMatDetails;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    @Override
    public String toString() {
        return "BillMaterial{" + "id=" + id + ", shopName=" + shopName + ", date=" + dateS + ", time=" + timeS + ", total=" + total + ", buy=" + buy + ", change=" + change + ", employee=" + employee + '}';
    }

    public void addBillMatDetail(BillDetail billMatDetail) {
        billMatDetails.add(billMatDetail);
        total += billMatDetail.getTotal();
    }

    public void addBillMatDetail(Material material, String matName, int amount, double price) {
        BillDetail billMatDetail = new BillDetail(material, matName, amount, price, total, this);
        addBillMatDetail(billMatDetail);
    }

    public void addBillMatDetail(Material material, int amount, double price) {
        BillDetail billMatDetail = new BillDetail(material, material.getName(), amount, price, total, this);
        addBillMatDetail(billMatDetail);
    }

    public static Bill fromRS(ResultSet rs) {
        Bill billMaterial = new Bill();
        EmployeeDao employeeDao = new EmployeeDao();
        try {
            billMaterial.setId(rs.getInt("bill_id"));
            billMaterial.setShopName(rs.getString("bill_shop_name"));
            int employeeId = rs.getInt("employee_id");
            Employee emp = employeeDao.get(employeeId);
            billMaterial.setEmployee(emp);
            billMaterial.setTotal(rs.getDouble("bill_total"));
            billMaterial.setBuy(rs.getDouble("bill_buy"));
            billMaterial.setChange(rs.getDouble("bill_change"));
            billMaterial.setDateS(rs.getString("bill_date"));
            billMaterial.setTimeS(rs.getString("bill_time"));
        } catch (SQLException ex) {
            Logger.getLogger(Bill.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return billMaterial;
    }
}
