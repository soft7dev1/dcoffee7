/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mukku.dcoffee7.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Acer
 */
public class EmployeeOverdue {

    private int empId;
    private String empName;
    private String totalHour;
    private double hourlyWage;
    private double salary;

    public EmployeeOverdue(int empId, String empName, String totalHour, double hourlyWage, double salary) {
        this.empId = empId;
        this.empName = empName;
        this.totalHour = totalHour;
        this.hourlyWage = hourlyWage;
        this.salary = salary;
    }

    public EmployeeOverdue() {
        this.empId = -1;
        this.empName = "";
        this.totalHour = "";
        this.hourlyWage = 0;
        this.salary = 0;
    }

    public int getEmpId() {
        return empId;
    }

    public void setEmpId(int empId) {
        this.empId = empId;
    }

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    public String getTotalHour() {
        return totalHour;
    }

    public void setTotalHour(String totalHour) {
        this.totalHour = totalHour;
    }

    public double getHourlyWage() {
        return hourlyWage;
    }

    public void setHourlyWage(double hourlyWage) {
        this.hourlyWage = hourlyWage;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "EmployeeOverdue{" + "empId=" + empId + ", empName=" + empName + ", totalHour=" + totalHour + ", hourlyWage=" + hourlyWage + ", salary=" + salary + '}';
    }

    public static EmployeeOverdue fromRS(ResultSet rs) {
        EmployeeOverdue item = new EmployeeOverdue();
        try {
            item.setEmpId(rs.getInt("employee_id"));
            item.setEmpName(rs.getString("employee_name"));
            item.setTotalHour(rs.getString("total_hour"));
            item.setHourlyWage(rs.getDouble("employee_hourly_wage"));
            item.setSalary(rs.getDouble("salary"));
        } catch (SQLException ex) {
            Logger.getLogger(EmployeeOverdue.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return item;
    }

}
