/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mukku.dcoffee7.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author user
 */
public class PopProductRp {

    private String name;
    private int amount;

    public PopProductRp(String name, int amount) {
        this.name = name;
        this.amount = amount;
    }

    public PopProductRp() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "PopProductRp{" + "name=" + name + ", amount=" + amount + '}';
    }
        public static PopProductRp fromRS(ResultSet rs) {
        PopProductRp pProdReport = new PopProductRp();
        try {
            pProdReport.setName(rs.getString("name"));
            pProdReport.setAmount(rs.getInt("pop_product"));
        } catch (SQLException ex) {
            Logger.getLogger(PopProductRp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return pProdReport;
    }

}
