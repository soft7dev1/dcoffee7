/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mukku.dcoffee7.model;

import com.mukku.dcoffee7.dao.MaterialDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author acer
 */
public class BillDetail {

    private int id;
    private Material material;
    private String matName;
    private int amount;
    private double price;
    private double total;
    private Bill billMaterial;

    public BillDetail(int id, Material material, String matName, int amount, double price, double total, Bill billMaterial) {
        this.id = id;
        this.material = material;
        this.matName = matName;
        this.amount = amount;
        this.price = price;
        this.total = total;
        this.billMaterial = billMaterial;
    }

    public BillDetail() {
        this.id = -1;
    }

    public BillDetail(Material material, String matName, int amount, double price, double total, Bill billMaterial) {
        this.material = material;
        this.matName = matName;
        this.amount = amount;
        this.price = price;
        this.total = total;
        this.billMaterial = billMaterial;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Material getMaterial() {
        return material;
    }

    public void setMaterial(Material material) {
        this.material = material;
    }

    public String getMatName() {
        return matName;
    }

    public void setMatName(String matName) {
        this.matName = matName;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getTotal() {
        return amount * price;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public Bill getBillMaterial() {
        return billMaterial;
    }

    public void setBillMaterial(Bill billMaterial) {
        this.billMaterial = billMaterial;
    }

    @Override
    public String toString() {
        return "BillMatDetail{" + "id=" + id + ", material=" + material + ", matName=" + matName + ", amount=" + amount + ", price=" + price + ", total=" + total + '}';
    }

    public static BillDetail fromRS(ResultSet rs) {
        MaterialDao materialDao = new MaterialDao();
        BillDetail billMatDt = new BillDetail();
        try {
            billMatDt.setId(rs.getInt("bill_detail_id"));
            int materialId = rs.getInt("mat_id");
            Material mat = materialDao.get(materialId);
            billMatDt.setMaterial(mat);
            billMatDt.setMatName(rs.getString("bill_detail_name"));
            billMatDt.setAmount(rs.getInt("bill_detail_amount"));
            billMatDt.setPrice(rs.getDouble("bill_detail_price"));
            billMatDt.setTotal(rs.getDouble("bill_detail_total"));
        } catch (SQLException ex) {
            Logger.getLogger(ChMaterialDetail.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return billMatDt;
    }
}
