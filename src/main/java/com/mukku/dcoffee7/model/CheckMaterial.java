/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mukku.dcoffee7.model;

import com.mukku.dcoffee7.dao.EmployeeDao;
import java.sql.ResultSet;
import java.sql.SQLException;
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.sqlite.date.FastDateFormat;
//import java.time.LocalTime

/**
 *
 * @author acer
 */
public class CheckMaterial {

    private int id;
    private Date date;
    private Date time;
    private String dateS;
    private String timeS;
    ArrayList<ChMaterialDetail> chMatDetails;
    private Employee employee;

    public CheckMaterial(int id, Date date, Date time, ArrayList<ChMaterialDetail> chMatDetails, Employee employee) {
        this.id = id;
        this.date = date;
        this.time = time;
        this.chMatDetails = chMatDetails;
        this.employee = employee;
    }

    public CheckMaterial() {
        this.id = -1;
    }

    public CheckMaterial(Employee employee) {
        this.id = -1;
        chMatDetails = new ArrayList<>();
        date = new Date();
        dateS = FastDateFormat.getInstance("dd/MM/yyyy").format(date);
        time = new Date();
        timeS = FastDateFormat.getInstance("HH:mm").format(time);
        this.employee = employee;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getDateS() {
        return dateS;
    }

    public void setDateS(String dateS) {
        this.dateS = dateS;
    }

    public String getTimeS() {
        return timeS;
    }

    public void setTimeS(String timeS) {
        this.timeS = timeS;
    }

    public ArrayList<ChMaterialDetail> getChMatDetails() {
        return chMatDetails;
    }

    public void setChMatDetails(ArrayList<ChMaterialDetail> chMatDetails) {
        this.chMatDetails = chMatDetails;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    @Override
    public String toString() {
        return "CheckMaterial{" + "id=" + id + ", date=" + dateS + ", time=" + timeS + ", employee=" + employee + '}';
    }

    public void addChMatDetail(ChMaterialDetail chMatDetail) {
        chMatDetails.add(chMatDetail);
    }

    public void addChMatDetail(Material material, String matName, int qtyLast, int qtyRemain) {
        ChMaterialDetail ChMatDetail = new ChMaterialDetail(material, matName, qtyLast, qtyRemain, this);
        this.addChMatDetail(ChMatDetail);
    }

    public void addChMatDetail(Material material, int qtyRemain) {
        ChMaterialDetail ChMatDetail = new ChMaterialDetail(material, material.getName(), material.getQuantity(), qtyRemain, this);
        this.addChMatDetail(ChMatDetail);
    }

    public static CheckMaterial fromRS(ResultSet rs) {
        CheckMaterial chMaterial = new CheckMaterial();
        EmployeeDao employeeDao = new EmployeeDao();
        try {
            chMaterial.setId(rs.getInt("check_mat_id"));
            int employeeId = rs.getInt("employee_id");
            Employee emp = employeeDao.get(employeeId);
            chMaterial.setEmployee(emp);
//            SimpleDateFormat sdfDate = new SimpleDateFormat("dd/MM/yyyy");
//            chMaterial.setDate(sdfDate.parse(rs.getString("check_mat_date")));
//            SimpleDateFormat sdfTime = new SimpleDateFormat("HH:mm");
//            chMaterial.setTime(sdfTime.parse(rs.getString("check_mat_time")));

//            chMaterial.setDate(FastDateFormat.getInstance("dd/MM/yyyy").parse(rs.getString("check_mat_date")));
//            chMaterial.setTime(FastDateFormat.getInstance("HH:mm").parse(rs.getString("check_mat_time")));
            chMaterial.setDateS(rs.getString("check_mat_date"));
//                System.out.println("get "+rs.getString("check_mat_date"));
            chMaterial.setTimeS(rs.getString("check_mat_time"));
//                System.out.println("get "+rs.getString("check_mat_time"));
        } catch (SQLException ex) {
            Logger.getLogger(CheckMaterial.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } //catch (ParseException ex) {
//            Logger.getLogger(CheckMaterial.class.getName()).log(Level.SEVERE, null, ex);
//        }
        return chMaterial;
    }
}
