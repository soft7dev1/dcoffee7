/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mukku.dcoffee7.model;

import com.mukku.dcoffee7.dao.CategoryDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author user
 */
public class Product {

    private int id;
    private String name;
    private String type;
    private String size;
    private double price;
    private Category category_id;

    public Product() {
        this.id = -1;
        this.type = "HIS";
        this.size = "SML";
    }

    public Product(int id, String name, String type, String size, double price, Category category_id) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.size = size;
        this.price = price;
        this.category_id = category_id;
    }

    public Product(String name, String type, String size, double price, Category category_id) {
        this.id = -1;
        this.name = name;
        this.type = type;
        this.size = size;
        this.price = price;
        this.category_id = category_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Category getCategory_id() {
        return category_id;
    }

    public void setCategory_id(Category category_id) {
        this.category_id = category_id;
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", name=" + name + ", type=" + type + ", size=" + size + ", price=" + price + ", category_id=" + category_id + '}';
    }

    public static Product fromRS(ResultSet rs) {
        CategoryDao categoryDao = new CategoryDao();
        Product product = new Product();
        try {
            product.setId(rs.getInt("product_id"));
            product.setName(rs.getString("product_name"));
            product.setType(rs.getString("product_type"));
            product.setSize(rs.getString("product_size"));
            product.setPrice(rs.getDouble("product_price"));

            int categoryId = rs.getInt("category_id");
            Category item = categoryDao.get(categoryId);
            product.setCategory_id(item);

        } catch (SQLException ex) {
            Logger.getLogger(Product.class.getName()).log(Level.SEVERE, null, ex);
            return null;

        }
        return product;
    }
}
