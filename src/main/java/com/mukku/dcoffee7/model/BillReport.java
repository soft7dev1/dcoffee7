/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mukku.dcoffee7.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author acer
 */
public class BillReport {

    private String matName;
    private int amount;
    private double total;

    public BillReport(String matName, int amount, double total) {
        this.matName = matName;
        this.amount = amount;
        this.total = total;
    }

    public BillReport() {
    }

    public String getMatName() {
        return matName;
    }

    public void setMatName(String matName) {
        this.matName = matName;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    @Override
    public String toString() {
        return "BillReport{" + "matName=" + matName + ", amount=" + amount + ", total=" + total + '}';
    }

    public static BillReport fromRS(ResultSet rs) {
        BillReport billReport = new BillReport();
        try {
            billReport.setMatName(rs.getString("bill_detail_name"));
            billReport.setAmount(rs.getInt("amount"));
            billReport.setTotal(rs.getInt("total"));
        } catch (SQLException ex) {
            Logger.getLogger(BillReport.class.getName()).log(Level.SEVERE, null, ex);
        }
        return billReport;
    }
}
