/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mukku.dcoffee7.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Acer
 */
public class SaleReport {
    String day;
    String month;
    String year;
    double total;
    public SaleReport(String day,String month,String year, double total) {
        this.day = day;
        this.month=month;
        this.year=year;
        this.total = total;
    }

    public SaleReport(String month, String year, double total) {
        this.day = "";
        this.month = month;
        this.year = year;
        this.total = total;
    }

    public SaleReport(String year, double total) {
        this.day = "";
         this.month = "";
        this.year = year;
        this.total = total;
    }

    public SaleReport() {
    }
    public String getDay() {
        return day;
    }
    public String getMonth() {
        return month;
    }
    public String getYear() {
        return year;
    }
    public double getTotal() {
        return total;
    }
    public void setDay(String day) {
        this.day = day;
    }
    public void setMonth(String month) {
        this.month = month;
    }
    public void setYear(String year) {
        this.year = year;
    }
    public void setTotal(double total) {
        this.total = total;
    }
    @Override
    public String toString() {
        return "ReportSale{" + "Date=" + day + ", total=" + total + '}';
    }
    public static SaleReport fromRSDay(ResultSet rs) {
            SaleReport obj=new SaleReport();
        try {
            obj.setDay(rs.getString("day"));
            obj.setTotal(rs.getDouble("total"));
            return obj;
        } catch (SQLException ex) {
            Logger.getLogger(SaleReport.class.getName()).log(Level.SEVERE, null, ex);
        }
        return obj;
        }
        
        public static SaleReport fromRSMonth(ResultSet rs) {
            SaleReport obj=new SaleReport();
        try {
            obj.setDay(rs.getString("month"));
            obj.setTotal(rs.getDouble("total"));
            return obj;
        } catch (SQLException ex) {
            Logger.getLogger(SaleReport.class.getName()).log(Level.SEVERE, null, ex);
        }
        return obj;
        }

        public static SaleReport fromRSYear(ResultSet rs) {
            SaleReport obj=new SaleReport();
        try {
            obj.setDay(rs.getString("year"));
            obj.setTotal(rs.getDouble("total"));
            return obj;
        } catch (SQLException ex) {
            Logger.getLogger(SaleReport.class.getName()).log(Level.SEVERE, null, ex);
        }
        return obj;
        }
}
