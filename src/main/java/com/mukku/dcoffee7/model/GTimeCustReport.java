/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mukku.dcoffee7.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author acer
 */
public class GTimeCustReport {

    private String time;
    private int numPeople;

    public GTimeCustReport(String time, int numPeople) {
        this.time = time;
        this.numPeople = numPeople;
    }

    public GTimeCustReport() {
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getNumPeople() {
        return numPeople;
    }

    public void setNumPeople(int numPeople) {
        this.numPeople = numPeople;
    }

    @Override
    public String toString() {
        return "GTimeCustReport{" + "time=" + time + ", numPeople=" + numPeople + '}';
    }

    public static GTimeCustReport fromRS(ResultSet rs) {
        GTimeCustReport gtcReport = new GTimeCustReport();
        try {
            gtcReport.setTime(rs.getString("time"));
            gtcReport.setNumPeople(rs.getInt("num_people"));
        } catch (SQLException ex) {
            Logger.getLogger(GTimeCustReport.class.getName()).log(Level.SEVERE, null, ex);
        }
        return gtcReport;
    }
}
