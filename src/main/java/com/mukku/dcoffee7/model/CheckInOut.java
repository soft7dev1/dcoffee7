/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mukku.dcoffee7.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Acer
 */
public class CheckInOut {
    private int id;
    private  String date;
    private  String timeIn;
    private  String timeOut;
    private  String totalHour;
    private  int empId;
    private  int ssId;

    public CheckInOut(int id, String date, String timeIn, String timeOut, String totalTime, int empId, int ssId) {
        this.id = id;
        this.date = date;
        this.timeIn = timeIn;
        this.timeOut = timeOut;
        this.totalHour = totalTime;
        this.empId = empId;
        this.ssId = ssId;
    }

    public CheckInOut() {
        this.id = -1;
        this.date = "";
        this.timeIn = "";
        this.timeOut = "";
        this.totalHour = "";
        this.empId = -1;
        this.ssId = -1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTimeIn() {
        return timeIn;
    }

    public void setTimeIn(String timeIn) {
        this.timeIn = timeIn;
    }

    public String getTimeOut() {
        return timeOut;
    }

    public void setTimeOut(String timeOut) {
        this.timeOut = timeOut;
    }

    public String getTotalHour() {
        return totalHour;
    }

    public void setTotalHour(String totalTime) {
        this.totalHour = totalTime;
    }

    public int getEmpId() {
        return empId;
    }

    public void setEmpId(int empId) {
        this.empId = empId;
    }

    public int getSsId() {
        return ssId;
    }

    public void setSsId(int ssId) {
        this.ssId = ssId;
    }

    @Override
    public String toString() {
        return "CheckInOut{" + "id=" + id + ", date=" + date + ", timeIn=" + timeIn + ", timeOut=" + timeOut + ", totalHour=" + totalHour + ", empId=" + empId + ", ssId=" + ssId + '}';
    }
    
    public static CheckInOut fromRS(ResultSet rs) {
        CheckInOut obj = new CheckInOut();
        try {
            obj.setId(rs.getInt("cio_id"));
            obj.setDate(rs.getString("cio_date"));
            obj.setTimeIn(rs.getString("cio_time_in"));
            obj.setTimeOut(rs.getString("cio_time_out"));
            obj.setTotalHour(rs.getString("cio_total_hour"));
            obj.setEmpId(rs.getInt("employee_id"));
            obj.setSsId(rs.getInt("ss_id"));
        } catch (SQLException ex) {
            Logger.getLogger(CheckInOut.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return obj;
    }
}
