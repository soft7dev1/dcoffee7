/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mukku.dcoffee7.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Acer
 */
public class SummarySalary {

    private int id;
    private String date;
    private String workHour;
    private double salary;

    public SummarySalary(int id, String date, String workHour, double salary) {
        this.id = id;
        this.date = date;
        this.workHour = workHour;
        this.salary = salary;
    }

    public SummarySalary() {
        this.id = -1;
        this.date = "";
        this.workHour = "";
        this.salary = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getWorkHour() {
        return workHour;
    }

    public void setWorkHour(String workHour) {
        this.workHour = workHour;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "SummarySalary{" + "id=" + id + ", date=" + date + ", workHour=" + workHour + ", salary=" + salary + '}';
    }
    
    public static SummarySalary fromRS(ResultSet rs) {
        SummarySalary item = new SummarySalary();
        try {
            item.setId(rs.getInt("ss_id"));
            item.setDate(rs.getString("ss_date"));
            item.setWorkHour(rs.getString("ss_work_hour"));
            item.setSalary(rs.getDouble("ss_salary"));
        } catch (SQLException ex) {
            Logger.getLogger(SummarySalary.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return item;
    }
}
