/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mukku.dcoffee7.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author acer
 */
public class ChMatReport {

    private String matName;
    private int qtyMin;
    private int qtyRemain;

    public ChMatReport(String matName, int qtyMin, int qtyRemain) {
        this.matName = matName;
        this.qtyMin = qtyMin;
        this.qtyRemain = qtyRemain;
    }

    public ChMatReport() {
    }

    public String getMatName() {
        return matName;
    }

    public void setMatName(String matName) {
        this.matName = matName;
    }

    public int getQtyMin() {
        return qtyMin;
    }

    public void setQtyMin(int qtyMin) {
        this.qtyMin = qtyMin;
    }

    public int getQtyRemain() {
        return qtyRemain;
    }

    public void setQtyRemain(int qtyRemain) {
        this.qtyRemain = qtyRemain;
    }

    @Override
    public String toString() {
        return "ChMatReport{" + "matName=" + matName + ", qtyMin=" + qtyMin + ", qtyRemain=" + qtyRemain + '}';
    }

    public static ChMatReport fromRS(ResultSet rs) {
        ChMatReport chMatReport = new ChMatReport();
        try {
            chMatReport.setMatName(rs.getString("cmd_name"));
            chMatReport.setQtyMin(rs.getInt("mat_min_quantity"));
            chMatReport.setQtyRemain(rs.getInt("cmd_qty_remain"));
        } catch (SQLException ex) {
            Logger.getLogger(ChMatReport.class.getName()).log(Level.SEVERE, null, ex);
        }
        return chMatReport;
    }
}
