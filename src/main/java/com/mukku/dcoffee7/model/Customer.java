/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mukku.dcoffee7.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ทักช์ติโชค
 */
public class Customer {

    private int id;
    private String name;
    private String tel;
    private int point;

    public Customer(int id, String name, String tel, int point) {
        this.id = id;
        this.name = name;
        this.tel = tel;
        this.point = point;
    }

    public Customer(String name, String tel, int point) {
        this.id = -1;
        this.name = name;
        this.tel = tel;
        this.point = point;
    }

    public Customer() {
        this.id = -1;
        this.name = "";
        this.tel = "";
        this.point = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    @Override
    public String toString() {
        return "Customer{" + "id=" + id + ", name=" + name + ", tel=" + tel + ", point=" + point + '}';
    }

    public static Customer fromRS(ResultSet rs) {
        Customer customer = new Customer();
        try {
            customer.setId(rs.getInt("customer_id"));
            customer.setName(rs.getString("customer_name"));
            customer.setTel(rs.getString("customer_tel"));
            customer.setPoint(rs.getInt("customer_point"));
        } catch (SQLException ex) {
            Logger.getLogger(Customer.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return customer;
    }

}
