/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mukku.dcoffee7.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author acer
 */
public class EmpSalaryReport {
 private String time;
    private double summarysarary;

    public EmpSalaryReport(String time, int summarysarary) {
        this.time = time;
        this.summarysarary = summarysarary;
    }

    public EmpSalaryReport() {
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public double getSummarysarary() {
        return summarysarary;
    }

    public void setSummarysarary(double summarysarary) {
        this.summarysarary = summarysarary;
    }

    @Override
    public String toString() {
        return "EmpSalaryReport{" + "time=" + time + ", summarysarary=" + summarysarary + '}';
    }
   public static EmpSalaryReport formRSDay(ResultSet rs){
        EmpSalaryReport empsReport = new EmpSalaryReport();
            try {
            empsReport.setTime(rs.getString("day"));
            empsReport.setSummarysarary(rs.getDouble("total"));
        } catch (SQLException ex) {
            Logger.getLogger(EmpSalaryReport.class.getName()).log(Level.SEVERE, null, ex);
        }
        return empsReport;
    }
     public static EmpSalaryReport formRSMonth(ResultSet rs){
        EmpSalaryReport empsReport = new EmpSalaryReport();
            try {
            empsReport.setTime(rs.getString("month"));
            empsReport.setSummarysarary(rs.getDouble("total"));
        } catch (SQLException ex) {
            Logger.getLogger(EmpSalaryReport.class.getName()).log(Level.SEVERE, null, ex);
        }
        return empsReport;
    }
      public static EmpSalaryReport formRSYear(ResultSet rs){
        EmpSalaryReport empsReport = new EmpSalaryReport();
            try {
            empsReport.setTime(rs.getString("year"));
            empsReport.setSummarysarary(rs.getDouble("total"));
        } catch (SQLException ex) {
            Logger.getLogger(EmpSalaryReport.class.getName()).log(Level.SEVERE, null, ex);
        }
        return empsReport;
    }
}
