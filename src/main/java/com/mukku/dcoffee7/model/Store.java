/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mukku.dcoffee7.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author acer
 */
public class Store {
     private int Id;
    private String Name;
    private String address;
    private String tel;
        public Store(int Id, String Name, String address, String tel) {
        this.Id = Id;
        this.Name = Name;
        this.address = address;
        this.tel = tel;
    }
        public Store(String Name, String address, String tel) {
        this.Id = -1;
        this.Name = Name;
        this.address = address;
        this.tel = tel;
    }
public Store() {
        this.Id = -1;
        this.Name = "";
        this.address = "";
        this.tel = "";
    }
 public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
        
    }
    @Override
    public String toString() {
        return "Store{" + "Id=" + Id + ", Name=" + Name + ", address=" + address + ", tel=" + tel + '}';
    }
    public static Store fromRS(ResultSet rs) {
      //  EmployeeDao employeeDao=new EmployeeDao();
        Store store = new Store();
        try {
            store.setId(rs.getInt("store_id"));
            store.setName(rs.getString("store_name"));
            store.setAddress(rs.getString("store_address"));
            store.setTel(rs.getString("store_tel"));
           
            

            
        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return store;
    }
}
