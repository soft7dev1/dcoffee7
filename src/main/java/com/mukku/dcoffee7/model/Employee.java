/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mukku.dcoffee7.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Acer
 */
public class Employee {

    private int id;
    private String name;
    private String address;
    private String telPhone;
    private String email;
    private String role;
    private double hourlyWage;
    private int userId;

    public Employee(int id, String name, String address, String telPhone, String email, String position, double hourlyWage, int userId) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.telPhone = telPhone;
        this.email = email;
        this.role = position;
        this.hourlyWage = hourlyWage;
        this.userId = userId;
    }

    public Employee() {
        this.id = -1;
        this.name = "";
        this.address = "";
        this.telPhone = "";
        this.email = "";
        this.role = "full time";
        this.hourlyWage = 0;
        this.userId = -1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTelPhone() {
        return telPhone;
    }

    public void setTelPhone(String telPhone) {
        this.telPhone = telPhone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String position) {
        this.role = position;
    }

    public double getHourlyWage() {
        return hourlyWage;
    }

    public void setHourlyWage(double hourlyWage) {
        this.hourlyWage = hourlyWage;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "Employee{" + "id=" + id + ", name=" + name + ", address=" + address + ", telPhone=" + telPhone + ", email=" + email + ", role=" + role + ", hourlyWage=" + hourlyWage + ", userId=" + userId + '}';
    }

    public static Employee fromRS(ResultSet rs) {
        Employee item = new Employee();
        try {
            item.setId(rs.getInt("employee_id"));
            item.setName(rs.getString("employee_name"));
            item.setAddress(rs.getString("employee_address"));
            item.setTelPhone(rs.getString("employee_tel"));
            item.setEmail(rs.getString("employee_email"));
            item.setRole(rs.getString("employee_position"));
            item.setHourlyWage(rs.getDouble("employee_hourly_wage"));
            item.setUserId(rs.getInt("user_id"));
        } catch (SQLException ex) {
            Logger.getLogger(Employee.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return item;
    }
}
