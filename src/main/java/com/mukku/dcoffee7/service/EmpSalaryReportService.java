/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mukku.dcoffee7.service;

import com.mukku.dcoffee7.dao.BillRpDao;
import com.mukku.dcoffee7.dao.EmpSalaryReportDao;
import com.mukku.dcoffee7.model.EmpSalaryReport;
import java.util.List;

/**
 *
 * @author acer
 */
public class EmpSalaryReportService {
    public List <EmpSalaryReport> getempSalaryReportbyday(String day){
         EmpSalaryReportDao empsDao = new EmpSalaryReportDao();
         return empsDao.getempsDayReports(day);
    }
        public List <EmpSalaryReport> getempSalaryReportbymonth(int year){
         EmpSalaryReportDao empsDao = new EmpSalaryReportDao();
         return empsDao.getempsMonthReports(year);
    }
          public List<EmpSalaryReport> getempSalaryReportbyear() {
       EmpSalaryReportDao empsDao = new EmpSalaryReportDao();
        return empsDao.getempsYearReports();
    }
//         public List<String> empsReportGetday() {
//       EmpSalaryReportDao empsDao = new EmpSalaryReportDao();
//        return empsDao.getDay();
//    } 
         public List<String> empsReportGetmonth() {
       EmpSalaryReportDao empsDao = new EmpSalaryReportDao();
        return empsDao.getMonth();
    }
                  public List<String> empsReportGetyear() {
       EmpSalaryReportDao empsDao = new EmpSalaryReportDao();
        return empsDao.getYear();
    }
}

