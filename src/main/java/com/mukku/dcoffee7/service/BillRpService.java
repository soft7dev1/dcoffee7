/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mukku.dcoffee7.service;

import com.mukku.dcoffee7.dao.BillRpDao;
import com.mukku.dcoffee7.model.BillReport;
import java.util.List;

/**
 *
 * @author acer
 */
public class BillRpService {

    public List<BillReport> billReportByMonth(int month) {
        BillRpDao billRpDao = new BillRpDao();
        return billRpDao.getBillMonthReport(month);
    }

    public List<BillReport> billReportByYear(int year) {
        BillRpDao billRpDao = new BillRpDao();
        return billRpDao.getBillYearReport(year);
    }

    public List<String> billReportGetMonth() {
        BillRpDao billRpDao = new BillRpDao();
        return billRpDao.getMonth();
    }

    public List<String> billReportGetYear() {
        BillRpDao billRpDao = new BillRpDao();
        return billRpDao.getYear();
    }
}
