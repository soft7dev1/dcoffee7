/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mukku.dcoffee7.service;

import com.mukku.dcoffee7.dao.ChMatRpDao;
import com.mukku.dcoffee7.model.ChMatReport;
import java.util.List;

/**
 *
 * @author acer
 */
public class ChMatRpService {

    public List<ChMatReport> chMatReportById(int chMatId) {
        ChMatRpDao chMatRpDao = new ChMatRpDao();
        return chMatRpDao.getNearlyOutReport(chMatId);
    }
}
