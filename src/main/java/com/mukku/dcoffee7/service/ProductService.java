/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mukku.dcoffee7.service;

import com.mukku.dcoffee7.dao.CategoryDao;
import com.mukku.dcoffee7.dao.ProductDao;
import com.mukku.dcoffee7.model.Category;
import com.mukku.dcoffee7.model.Product;
import java.util.List;

/**
 *
 * @author user
 */
public class ProductService {

    public List<Product> getProduct() {
        ProductDao productDao = new ProductDao();
        return productDao.getAll("product_id asc");
    }

    public Category getCatId(String catId) {
        CategoryDao catDao = new CategoryDao();
        return catDao.get(Integer.parseInt(catId));
    }

    public Product addNew(Product editedProduct) {
        ProductDao productDao = new ProductDao();
        return productDao.save(editedProduct);
    }

    public Product update(Product editedProduct) {
        ProductDao productDao = new ProductDao();
        return productDao.update(editedProduct);
    }

    public int delete(Product editedProduct) {
        ProductDao productDao = new ProductDao();
        return productDao.delete(editedProduct);
    }
}
