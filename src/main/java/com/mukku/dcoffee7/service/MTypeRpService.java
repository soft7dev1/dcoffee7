/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mukku.dcoffee7.service;

import com.mukku.dcoffee7.dao.MTCusRpDao;
import com.mukku.dcoffee7.model.MTypeCusReport;
import java.util.List;

/**
 *
 * @author ทักช์ติโชค
 */
public class MTypeRpService {

    public List<MTypeCusReport> getMTypeCusReport() {
        MTCusRpDao mtcRpDao = new MTCusRpDao();
        return mtcRpDao.getTypeCusReport();
    }

}
