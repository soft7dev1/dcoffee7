/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mukku.dcoffee7.service;

import com.mukku.dcoffee7.dao.CheckMatDao;
import com.mukku.dcoffee7.dao.EmployeeDao;
import com.mukku.dcoffee7.model.CheckMaterial;
import com.mukku.dcoffee7.model.Employee;
import java.util.List;

/**
 *
 * @author acer
 */
public class CheckMatService {

    public List<CheckMaterial> getCheckMaterials() {
        CheckMatDao checkMatDao = new CheckMatDao();
        return checkMatDao.getAll();
    }

    public CheckMaterial addCheckMaterial(CheckMaterial editedCheckMaterial) {
        CheckMatDao checkMatDao = new CheckMatDao();
        return checkMatDao.save(editedCheckMaterial);
    }

    public CheckMaterial updateCheckMaterial(CheckMaterial editedCheckMaterial) {
        CheckMatDao checkMatDao = new CheckMatDao();
        return checkMatDao.update(editedCheckMaterial);
    }

    public int deleteCheckMaterial(CheckMaterial editedCheckMaterial) {
        CheckMatDao checkMatDao = new CheckMatDao();
        return checkMatDao.delete(editedCheckMaterial);
    }

    public Employee getChMatEmp(String empId) {
        EmployeeDao empDao = new EmployeeDao();
        return empDao.get(Integer.parseInt(empId));
    }
}
