/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mukku.dcoffee7.service;

import com.mukku.dcoffee7.dao.CustomerDao;
import com.mukku.dcoffee7.dao.EmployeeDao;
import com.mukku.dcoffee7.dao.RecieptDao;
import com.mukku.dcoffee7.dao.StoreDao;
import com.mukku.dcoffee7.model.Customer;
import com.mukku.dcoffee7.model.Employee;
import com.mukku.dcoffee7.model.Reciept;
import com.mukku.dcoffee7.model.Store;
import java.util.List;

/**
 *
 * @author acer
 */
public class RecieptService {
     public List<Reciept> getReciept() {
        RecieptDao recieptDao = new RecieptDao();
        return recieptDao.getAll();
    }

    public Reciept addReciept(Reciept editedReciept) {
       RecieptDao recieptDao = new RecieptDao();
        return recieptDao.save(editedReciept);
    }
    public int deleteReciept(Reciept editedReciept) {
        RecieptDao recieptDao = new RecieptDao();
        return recieptDao.delete(editedReciept);
    }

    public Store getRecStore(String stId) {
        StoreDao storeDao = new StoreDao();
        return storeDao.get(Integer.parseInt(stId));
    }
      public Employee getRecEmp(String empId) {
        EmployeeDao empDao = new EmployeeDao();
        return empDao.get(Integer.parseInt(empId));
    }
      public Customer getRecCus(String cusId) {
       CustomerDao cusDao = new CustomerDao();
        return cusDao.get(Integer.parseInt(cusId));
    }
    public Reciept updateReciept(Reciept editedReciept) {
        RecieptDao recieptDao = new RecieptDao();  
         return recieptDao.update(editedReciept);
}
}
