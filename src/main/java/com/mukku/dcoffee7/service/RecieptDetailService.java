/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mukku.dcoffee7.service;


import com.mukku.dcoffee7.dao.ProductDao;
import com.mukku.dcoffee7.dao.RecieptDao;
import com.mukku.dcoffee7.dao.RecieptDetailDao;
import com.mukku.dcoffee7.model.Product;
import com.mukku.dcoffee7.model.Reciept;
import com.mukku.dcoffee7.model.RecieptDetail;
import java.util.List;

/**
 *
 * @author acer
 */
public class RecieptDetailService {
     public List<RecieptDetail> getRecieptDetail(int selectIndex) {
        RecieptDetailDao recieptDetailDao = new RecieptDetailDao();
        return recieptDetailDao.getbyrecieptID(selectIndex);
    }
    public RecieptDetail addRecieptDetail(RecieptDetail editedRecieptDetail) {
      RecieptDetailDao recieptDetailDao = new RecieptDetailDao();
        return recieptDetailDao.save(editedRecieptDetail);
    }

      public RecieptDetail updateRecieptDetail(RecieptDetail editedRecieptDetail) {
        RecieptDetailDao recieptDetailDao = new RecieptDetailDao();
        return recieptDetailDao.update(editedRecieptDetail);
    }
     public int deleteRecieptDetail(RecieptDetail editedRecieptDetail) {
        RecieptDetailDao recieptDetailDao = new RecieptDetailDao();
        return recieptDetailDao.delete(editedRecieptDetail);
    }
    public Reciept getReciept(String recId) {
       RecieptDao recDao = new RecieptDao();
        return recDao.get(Integer.parseInt(recId));
    }

   public Product getProduct(String proId) {
        ProductDao productDaoDao = new ProductDao();
        return productDaoDao.get(Integer.parseInt(proId));
    }
           public Reciept updateRecieptTotal(int redId) {
        RecieptDao recDao = new RecieptDao();
        Reciept  rec = recDao.get(redId);
        double total = 0;
        for (RecieptDetail b : getRecieptDetail(redId)) {
            total += b.getTotal();
        }
        rec.setTotal(total);
        return recDao.update(rec);
    }
}
