/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mukku.dcoffee7.service;

import com.mukku.dcoffee7.dao.CategoryDao;
import com.mukku.dcoffee7.model.Category;
import java.util.List;

/**
 *
 * @author user
 */
public class CategoryService {

    public List<Category> getCategory() {
        CategoryDao categoryDao = new CategoryDao();
        return categoryDao.getAll("category_id asc");
    }

    public Category addNew(Category editedCategory) {
        CategoryDao categoryDao = new CategoryDao();
        return categoryDao.save(editedCategory);
    }

    public Category update(Category editedCategory) {
        CategoryDao categoryDao = new CategoryDao();
        return categoryDao.update(editedCategory);
    }

    public int delete(Category editedCategory) {
        CategoryDao categoryDao = new CategoryDao();
        return categoryDao.delete(editedCategory);
    }

}
