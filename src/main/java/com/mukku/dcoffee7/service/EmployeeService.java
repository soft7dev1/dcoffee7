/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mukku.dcoffee7.service;

import com.mukku.dcoffee7.dao.EmployeeDao;
import com.mukku.dcoffee7.model.Employee;
import java.util.List;

/**
 *
 * @author Acer
 */
public class EmployeeService {
    public List<Employee> getEmployees() {
        EmployeeDao empDao = new EmployeeDao();
        return empDao.getAll();
    }

    public Employee addNew(Employee editedEmployee) {
        EmployeeDao empDao = new EmployeeDao();
        return empDao.save(editedEmployee);
    }

    public Employee update(Employee editedEmployee) {
        EmployeeDao empDao = new EmployeeDao();
        return empDao.update(editedEmployee);
    }

    public int delete(Employee editedEmployee) {
        EmployeeDao empDao = new EmployeeDao();
        return empDao.delete(editedEmployee);
    }
}
