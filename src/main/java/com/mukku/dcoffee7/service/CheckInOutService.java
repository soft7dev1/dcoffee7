/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mukku.dcoffee7.service;

import com.mukku.dcoffee7.dao.CheckInOutDao;
import com.mukku.dcoffee7.model.CheckInOut;
import java.util.List;

/**
 *
 * @author Acer
 */
public class CheckInOutService {

    public List<CheckInOut> getCheckInOut() {
        CheckInOutDao cioDao = new CheckInOutDao();
        return cioDao.getAll();
    }

    public CheckInOut addNew(CheckInOut editedCheckInOut) {
        CheckInOutDao cioDao = new CheckInOutDao();
        return cioDao.save(editedCheckInOut);
    }

    public CheckInOut update(CheckInOut editedCheckInOut) {
        CheckInOutDao cioDao = new CheckInOutDao();
        return cioDao.update(editedCheckInOut);
    }

    public CheckInOut getInOutTime(int emp_id) {
        CheckInOutDao cioDao = new CheckInOutDao();
        return cioDao.getInOutTime(emp_id);
    }

    public CheckInOut updateSsId(CheckInOut editedCheckInOut, int emp_id) {
        CheckInOutDao cioDao = new CheckInOutDao();
        return cioDao.updateSsId(editedCheckInOut, emp_id);
    }

    public CheckInOut updateNullSsId(CheckInOut editedCheckInOut) {
        CheckInOutDao cioDao = new CheckInOutDao();
        return cioDao.updateNullSsId(editedCheckInOut);
    }
}
