/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mukku.dcoffee7.service;


import com.mukku.dcoffee7.dao.PopProdRpDao;
import com.mukku.dcoffee7.model.PopProductRp;
import java.util.List;

/**
 *
 * @author user
 */
public class PProdRpService {

    public List<PopProductRp> getPProductReport() {
        PopProdRpDao popProdRpDao = new PopProdRpDao();
        return popProdRpDao.getPopProductReport();
    }
}
