/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mukku.dcoffee7.service;

import com.mukku.dcoffee7.dao.SaleReportDao;
import com.mukku.dcoffee7.model.SaleReport;
import java.util.List;

/**
 *
 * @author Acer
 */
public class SaleReportService {
    public List<SaleReport> ReportSaleByDay(String month) {
        SaleReportDao dao =new SaleReportDao();
        return dao.getDayReport(month);
    }
    
    public List<SaleReport> ReportSaleByMonth(int year) {
        SaleReportDao dao =new SaleReportDao();
        return dao.getMonthReport(year);
    }

    public List<SaleReport> ReportSaleByYear() {
        SaleReportDao dao =new SaleReportDao();
        return dao.getYearReport();
    }
    public List<String> getReportSaleByDay(){
        SaleReportDao dao =new SaleReportDao();
       return dao.getDay();
    }
    public List<String> getReportSaleByMonth(){
        SaleReportDao dao =new SaleReportDao();
       return dao.getMonth();
    }
    public List<String> getReportSaleByYear(){
        SaleReportDao dao =new SaleReportDao();
       return dao.getYear();
    }
}
