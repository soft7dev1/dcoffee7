/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mukku.dcoffee7.service;

import com.mukku.dcoffee7.dao.StoreDao;
import com.mukku.dcoffee7.dao.UserDao;
import com.mukku.dcoffee7.model.Store;
import com.mukku.dcoffee7.model.User;
import java.util.List;

/**
 *
 * @author acer
 */
public class StoreService {
    
  
    
     public List<Store> getStore(){
      StoreDao storeDao = new StoreDao();
      return storeDao.getAll();
  }
    public Store addNew(Store editedStore) {
     StoreDao storeDao = new StoreDao();
        return  storeDao.save(editedStore);
    }
    public Store update(Store editedStore) {
       StoreDao storeDao = new StoreDao();
        return  storeDao.update(editedStore);
    }
    public int delete(Store editedStore) {
        StoreDao storeDao = new StoreDao();
        return storeDao.delete(editedStore);
    }
}

    
    
    

