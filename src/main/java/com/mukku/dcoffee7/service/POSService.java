/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mukku.dcoffee7.service;

import com.mukku.dcoffee7.dao.CustomerDao;
import com.mukku.dcoffee7.dao.POSDao;
import com.mukku.dcoffee7.model.Customer;
import java.text.DecimalFormat;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.JTextField;

/**
 *
 * @author acer
 */
public class POSService {

    POSDao posDao = new POSDao();
    private static final DecimalFormat df0 = new DecimalFormat("0");
    private CustomerDao customerDao;
    private Customer customer;

    public int findTel(JTextField edtFindTel) {
        return posDao.findTel(edtFindTel);
    }

    public int runQueue(JLabel txtDate) {
        return posDao.runQueue(txtDate);
    }

    public double totalFromTb(JTable tblOrder) {
        double total = 0;
        for (int i = 0; i < tblOrder.getRowCount(); i++) {
            if (tblOrder.getValueAt(i, 3) != null) {
                total += Double.parseDouble("" + tblOrder.getValueAt(i, 3));
            }
        }
        return total;
    }

    public int getPoint(int id) {
        customerDao = new CustomerDao();
        return customerDao.get(id).getPoint();
    }

//    public int givenPoint(double total, JLabel txtCusId) {//throws NumberFormatException
//        if (!txtCusId.getText().equals("-1")) {
//            double point = Math.floor(total / 10);
//            customerDao = new CustomerDao();
//            customer = customerDao.get(Integer.parseInt(txtCusId.getText()));
////            customer.setPoint(Integer.parseInt(df0.format(point)) + customer.getPoint());
//            System.out.println(df0.format(point));
//            return Integer.parseInt(df0.format(point)) + customer.getPoint();
////            customerDao.update(customer);
//        }
//        return 0;
//    }

    public void upCusPoint(String point, JLabel txtCusId) {//throws NumberFormatException
        if (!txtCusId.getText().equals("-1")) {
            customerDao = new CustomerDao();
            customer = customerDao.get(Integer.parseInt(txtCusId.getText()));
            customer.setPoint(Integer.parseInt(point));
            customerDao.update(customer);
        }
    }

}
