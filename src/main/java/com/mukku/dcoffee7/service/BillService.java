/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mukku.dcoffee7.service;

import com.mukku.dcoffee7.dao.BillDao;
import com.mukku.dcoffee7.dao.EmployeeDao;
import com.mukku.dcoffee7.model.Bill;
import com.mukku.dcoffee7.model.Employee;
import java.util.List;

/**
 *
 * @author acer
 */
public class BillService {

    public List<Bill> getBills() {
        BillDao billDao = new BillDao();
        return billDao.getAll();
    }

    public Bill addBill(Bill editedBill) {
        BillDao billDao = new BillDao();
        return billDao.save(editedBill);
    }

    public Bill updateBill(Bill editedBill) {
        BillDao billDao = new BillDao();
        return billDao.update(editedBill);
    }

    public int deleteBill(Bill editedBill) {
        BillDao billDao = new BillDao();
        return billDao.delete(editedBill);
    }

    public Employee getBillEmp(String empId) {
        EmployeeDao empDao = new EmployeeDao();
        return empDao.get(Integer.parseInt(empId));
    }

}
