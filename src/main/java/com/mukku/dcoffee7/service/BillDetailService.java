/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mukku.dcoffee7.service;

import com.mukku.dcoffee7.dao.BillDao;
import com.mukku.dcoffee7.dao.BillDetailDao;
import com.mukku.dcoffee7.dao.MaterialDao;
import com.mukku.dcoffee7.model.Bill;
import com.mukku.dcoffee7.model.BillDetail;
import com.mukku.dcoffee7.model.Material;
import java.util.List;

/**
 *
 * @author acer
 */
public class BillDetailService {

    public List<BillDetail> getBillDetails(int selectIndex) {
        BillDetailDao billDetailDao = new BillDetailDao();
        return billDetailDao.getByBillMaterialID(selectIndex);
    }

    public BillDetail addBillDetail(BillDetail editedBillDetail) {
        BillDetailDao billDetailDao = new BillDetailDao();
        return billDetailDao.save(editedBillDetail);
    }

    public BillDetail updateBillDetail(BillDetail editedBillDetail) {
        BillDetailDao billDetailDao = new BillDetailDao();
        return billDetailDao.update(editedBillDetail);
    }

    public int deleteBillDetail(BillDetail editedBillDetail) {
        BillDetailDao billDetailDao = new BillDetailDao();
        return billDetailDao.delete(editedBillDetail);
    }

    public Bill updateBillTotal(int billId) {
        BillDao billDao = new BillDao();
        Bill bill = billDao.get(billId);
        double total = 0;
        for (BillDetail b : getBillDetails(billId)) {
            total += b.getTotal();
        }
        bill.setTotal(total);
        return billDao.update(bill);
    }

    public Material getBillDtMatr(String matId) {
        MaterialDao matDao = new MaterialDao();
        return matDao.get(Integer.parseInt(matId));
    }

    public Bill getBillDtBill(String billId) {
        BillDao billDao = new BillDao();
        return billDao.get(Integer.parseInt(billId));
    }

}
