/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mukku.dcoffee7.service;

import com.mukku.dcoffee7.dao.EmployeeOverdueDao;
import com.mukku.dcoffee7.model.EmployeeOverdue;
import java.util.List;

/**
 *
 * @author Acer
 */
public class EmployeeOverdueService {
    public List<EmployeeOverdue> getEmployeeOverdue() {
        EmployeeOverdueDao eoService = new EmployeeOverdueDao();
        return eoService.getAll();
    }
}
