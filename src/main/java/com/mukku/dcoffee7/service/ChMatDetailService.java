/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mukku.dcoffee7.service; //waiting Material Class

import com.mukku.dcoffee7.dao.ChMatDetailDao;
import com.mukku.dcoffee7.dao.CheckMatDao;
import com.mukku.dcoffee7.dao.MaterialDao;
//import com.mukku.dcoffee7.dao.MaterialDao;
import com.mukku.dcoffee7.model.ChMaterialDetail;
import com.mukku.dcoffee7.model.CheckMaterial;
import com.mukku.dcoffee7.model.Material;
//import com.mukku.dcoffee7.model.Material;
import java.util.List;

/**
 *
 * @author acer
 */
public class ChMatDetailService {

    public List<ChMaterialDetail> getChMaterialDetails(int selectIndex) {
        ChMatDetailDao chMatDetailDao = new ChMatDetailDao();
        return chMatDetailDao.getByChMaterialID(selectIndex);
    }

    public ChMaterialDetail addChMaterialDetail(ChMaterialDetail editedChMaterialDetail) {
        ChMatDetailDao checkMatDetailDao = new ChMatDetailDao();
        return checkMatDetailDao.save(editedChMaterialDetail);
    }

    public ChMaterialDetail updateChMaterialDetail(ChMaterialDetail editedChMaterialDetail) {
        ChMatDetailDao checkMatDetailDao = new ChMatDetailDao();
        return checkMatDetailDao.update(editedChMaterialDetail);
    }

    public int deleteChMaterialDetail(ChMaterialDetail editedChMaterialDetail) {
        ChMatDetailDao checkMatDetailDao = new ChMatDetailDao();
        return checkMatDetailDao.delete(editedChMaterialDetail);
    }

    public Material getChMatDtMatr(String matId) {
        MaterialDao matDao = new MaterialDao();
        return matDao.get(Integer.parseInt(matId));
    }

    public CheckMaterial getChMatDtChMat(String chMatId) {
        CheckMatDao checkMatDao = new CheckMatDao();
        return checkMatDao.get(Integer.parseInt(chMatId));
    }
}
