/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mukku.dcoffee7.service;

import com.mukku.dcoffee7.dao.GTCustRpDao;
import com.mukku.dcoffee7.dao.GTCustRpDao;
import com.mukku.dcoffee7.model.GTimeCustReport;
import java.util.List;

/**
 *
 * @author acer
 */
public class GTimeRpService {

    public List<GTimeCustReport> getGTimeCustReport() {
        GTCustRpDao gtcRpDao = new GTCustRpDao();
        return gtcRpDao.getGTimeReport();
    }
}
