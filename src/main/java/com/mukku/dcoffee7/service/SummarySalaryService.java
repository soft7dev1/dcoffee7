/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mukku.dcoffee7.service;

import com.mukku.dcoffee7.dao.SummarySalaryDao;
import com.mukku.dcoffee7.model.SummarySalary;
import java.util.List;

/**
 *
 * @author Acer
 */
public class SummarySalaryService {
    public List<SummarySalary> getSummarySalary() {
        SummarySalaryDao ssDao = new SummarySalaryDao();
        return ssDao.getAll();
    }
    
    public SummarySalary addNew(SummarySalary obj) {
        SummarySalaryDao ssDao = new SummarySalaryDao();
        return ssDao.save(obj);
    }
    
    public int delete(SummarySalary editedSummarySalary) {
        SummarySalaryDao ssDao = new SummarySalaryDao();
        return ssDao.delete(editedSummarySalary);
    }
}
